#pragma once

#include <iostream>
#include <memory>
#include <unordered_set>
#include <vector>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/util.h>
#include <event2/event.h>
#include "../inc/appaddress.h"
#include "../core/msgdecoder.h"
#include "../core/servicemanager.h"
#include "../inc/protobuf.h"
#include "../core/connection.h"
#include "../websocket/websocket_handler.h"

class SessionManager;
class BrokerServer;

using namespace std;

struct Response
{
	vector<AppAddress> vecDestAddr;
	PackageHeader header;
	string msg;
};
struct Task
{
	AppAddress srcAddr;
	PackagePtr request;
	vector<shared_ptr<Response>> vecResponse;
};


class Session
{
private:
	Connection* conn;
	SessionManager* connManager;
	BrokerServer* server;

	SockInfo sockInfo;
	AppAddress mAddr;
	bool hasLogin;
	bool hasSuperRights;
	bool stoped;
	std::mutex  mutexStop;
	MsgExpress::LoginInfo mInfo;
	unordered_set<int> mServiceList;//我提供的服务
	ServiceManager * mServiceManager;

	int mps;
	uint64_t lastCountTime;
	int msgCount;
public:
	Session(event_base* base, const SockInfo& sockinfo, SessionManager*, BrokerServer*, ServiceManager *);
	~Session();
	void setAddress(AppAddress);
	AppAddress getAddress();

	void handle(Task* task);
	void login(Task* task);
	void logout(Task* task);
	void regservice(Task* task);
	void heartBeat(Task* task);
	void subscribe(Task* task);
	void complexSubscribe(Task* task);
	void unsubscribe(Task* task);
	void getAppInfo(Task* task);

	void reply(Package* package, const google::protobuf::Message& replyMsg, vector<shared_ptr<Response>>* vecResponse);
	void reply(Package* package, unsigned int errCode, string errMsg, vector<shared_ptr<Response>>* vecResponse);

        void publish(AppAddress dstAddr, MessagePtr msg, vector<shared_ptr<Response>>* vecResponse);
	void publish(unsigned int cmd, unsigned int serial,AppAddress srcAddr, MsgExpress::PublishData* pubData, vector<shared_ptr<Response>>* vecResponse);
	void publish(unsigned int cmd, unsigned int serial, AppAddress srcAddr, Package* package, vector<shared_ptr<Response>>* vecResponse);
	void simplePublish(unsigned int cmd, unsigned int serial, AppAddress srcAddr, Package* package, vector<shared_ptr<Response>>* vecResponse);
	void complexPublish(unsigned int cmd, unsigned int serial, AppAddress srcAddr, Package* package, vector<shared_ptr<Response>>* vecResponse);

	void post(vector<shared_ptr<Response>>* vecResponse, const char * buffer, size_t size, AppAddress toAddr);
	void post(vector<shared_ptr<Response>>* vecResponse, const char * buffer, size_t size, unordered_set<AppAddress, AppAddressHash>* setIds);
	void sendMsg(const string& msg);

	void stop();
private:

	bool closed(){ return stoped;  }

public:
	void executeSend(void* arg);
public:
	//call back
	void onPackage(PackagePtr package);
	void onSend(bool needsend,void* arg);
	void onError(short err);
};

typedef shared_ptr<Session> SessionPtr;
