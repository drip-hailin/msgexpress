#include "broker.h"
#include "session.h"
#include <signal.h>
#include "../inc/logger.h"
#include "../core/messageutil.h"
#include "../core/command_config.h"
#include <thread>
#include <mutex>
#ifdef __linux__
#include <sys/prctl.h>
#include <stdio.h>  
#include <unistd.h>  
#include <sys/time.h>  
#include <string.h>  
#include <stdlib.h>  

#define VMRSS_LINE 17  
#define VMSIZE_LINE 13  
#define PROCESS_ITEM 14  
#endif

BrokerServer::BrokerServer(ServiceManager* manager, const string& cfgfile)
{
	isMaster = true;
	startTime = 0;
	startTime = time(&startTime);
	brokerId = 0;
	brokerHeartbeat = 0;
	mServiceManager = manager;
	base = nullptr;
	listener = nullptr;
	signal_event = nullptr;
	mIsShutdown = 0;
	mMaxThreads = 4;
	if (!sconf.LoadFile(cfgfile))
	{
		LOG4_ERROR("read server xml config file error");
	}
	if (sconf.thread_>0)
	    mMaxThreads = sconf.thread_;
	brokerId = sconf.brokerId;
	sessionManager.setBrokerId(brokerId);
}


BrokerServer::~BrokerServer()
{
}

AppAddress BrokerServer::getAddress()
{
	AppAddress addr(0);
	addr.Broker.Id = sconf.brokerId;
	return addr;
}

void BrokerServer::onAccept(evutil_socket_t fd, struct sockaddr * sa)
{
	sockaddr_in* pSin = (sockaddr_in*)sa;
	SockInfo sock;
	sock.sock = fd;
	snprintf(sock.ip, 16, inet_ntoa(pSin->sin_addr));
	sock.port = pSin->sin_port;
	Session* conn = new Session(base, sock, &sessionManager, this, mServiceManager);
}

void listener_cb(struct evconnlistener * listener, evutil_socket_t fd, struct sockaddr * sa, int socklen, void * user_data)
{
	BrokerServer* server = (BrokerServer*)user_data;
	server->onAccept(fd,sa);
}

void signal_cb(evutil_socket_t sig, short events, void *user_data)
{
	cout << sp_thread_self() << endl;
	BrokerServer* server = (BrokerServer*)user_data;
	LOG4_ERROR("Signal %d", events);
}

void cycle_timout_cb(evutil_socket_t fd, short event, void *arg)
{
	BrokerServer* server = (BrokerServer*)arg;
	server->onTimer();
}

void BrokerServer::noticeServer()
{
	//LOG4_INFO("Broker mode changed,now notice servers");
	std::vector<int> servicelist = mServiceManager->getMasterSlaveService();
	for (int i = 0; i < servicelist.size(); i++)
		publishAppServer(servicelist.at(i));
}

void BrokerServer::kickSession(int type)
{
	//type&0x1=1 踢所有客户端 type&0x2=2，踢所有非主从服务，type&0x4=4，踢所有主从服务
	MsgExpress::AppList list;
	mServiceManager->getAppList(list);
	LOG4_INFO("Now kick off the session,type=%d,appList:%s",type,list.ShortDebugString().c_str());
	
	for (int i = 0; i < list.appinfo_size(); i++)
	{
		SessionPtr connDest = sessionManager.getSession(list.appinfo().Get(i).addr());
		if (!connDest)
			continue;
		const MsgExpress::LoginInfo& info=list.appinfo().Get(i).logininfo();
		if (info.type() == 0 && (type&0x01)==1)
		{
			connDest->stop();
		}
		else if (info.type() == 1 )
		{
			unsigned int serviceId = 0;
			if (info.serviceid_size() > 0)
			    serviceId = info.serviceid(0);
			bool isMatserSlave = mServiceManager->isMasterSlaveMode(serviceId);
			if ((type & 0x02) == 2 && !isMatserSlave)
				connDest->stop();
			else if ((type & 0x04) == 4 && isMatserSlave)
				connDest->stop();
		}
		else if (info.type() == 3 && (type & 0x08))
		{
			connDest->stop();
		}
	}
}
void BrokerServer::kickAll()
{
	MsgExpress::AppList list;
	mServiceManager->getAppList(list);
	LOG4_INFO("Now kick off all the session,appList:%s", list.ShortDebugString().c_str());

	for (int i = 0; i < list.appinfo_size(); i++)
	{
		SessionPtr connDest = sessionManager.getSession(list.appinfo().Get(i).addr());
		if (!connDest)
			continue;
		connDest->stop();
	}
}
void BrokerServer::updateBrokerInfo(MsgExpress::BrokerInfo* info)
{
	if (sconf.clustermode != enumClusterMode::MasterSlave)
		return;
	if ((info->brokerid() < brokerId) && this->isMasterBroker())
	{
		isMaster = false;
		LOG4_INFO("I am a slave broker");
		//踢掉所有客户端和服务
		kickSession(7);
		//通知其他
		noticeServer();
	}
	else if (info->brokerid() > brokerId)
	{
		if (this->isMasterBroker())
		{
			if (brokerHeartbeat > 3)
			{
				//我本来就是master，但是broker之间掉线时间过长，我就把服务先踢下线重连一下,
				//LOG4_ERROR("Not receive heartbeat from slave broker for a long time,now notice service to reconnect broker.");
				//踢掉所有masterslave服务
				//kickSession(4);
			}
		}
		else
		{
			//我升级为master
			isMaster = true;
			LOG4_INFO("I am a master broker");
			noticeServer();
		}
	}
	brokerHeartbeat = 0;
}

void BrokerServer::publishBrokerInfo()
{
	brokerHeartbeat++;
	if (sconf.clustermode==enumClusterMode::MasterSlave && brokerHeartbeat > 3 && !this->isMasterBroker())
	{
		//三次以上没有收到其他broker的心跳，我就升级到master
		isMaster = true;
		LOG4_INFO("I am a master broker");
		noticeServer();
	}
	MsgExpress::BrokerInfo brokerInfo;

	//LOG4_INFO("PublishBrokerInfo:%s", brokerInfo.ShortDebugString().c_str());
	brokerInfo.set_brokerid(getConfig().brokerId);
	brokerInfo.set_starttime(getStartTime());
	brokerInfo.set_ismaster(isMasterBroker());
	string data;
	PackageHeader header;
	header.set_command(CommandConfig::getInstance().GetCommand(brokerInfo.GetTypeName().c_str()));// CommandConfig::getInstance().getCommand(brokerInfo.GetTypeName().c_str(), PackageHeader::Request, -1));
	header.set_type( PackageHeader::Publish);
	header.set_serialnum(0);
	header.set_srcaddr ( getAddress());
	header.set_dstaddr(0);
	MessageUtil::serializePackageToString(header, brokerInfo, true, 50, &data);
	unordered_set<AppAddress, AppAddressHash>* setIds = mServiceManager->getSubscriberIdList(header.command(), "");
	unordered_set<AppAddress, AppAddressHash>::const_iterator it = setIds->begin();
	while (it != setIds->end())
	{
		AppAddress dest = *it;
		SessionPtr connDest = sessionManager.getSession(dest);
		if (connDest)
			connDest->sendMsg(data);
		it++;
	}
}

void BrokerServer::publishAppServer(int serviceId)
{
	MsgExpress::GetAppList serviceInfo;
	serviceInfo.add_serviceid(serviceId);
	MsgExpress::AppList list;
	mServiceManager->getAppInfo(serviceInfo, list);
	MsgExpress::AppServerList appList;
	appList.set_serviceid(serviceId);
	appList.set_ismasterslavemode(mServiceManager->isMasterSlaveMode(serviceId));
	appList.set_brokerismaster(isMasterBroker());
	for (int i = 0; i < list.appinfo_size(); i++)
		appList.add_addrs(list.appinfo(i).addr());

	//LOG4_INFO("PublishAppServer:%s", appList.ShortDebugString().c_str());

	string data;
	PackageHeader header;
	header.set_command( CommandConfig::getInstance().GetCommand(appList.GetTypeName().c_str()));
	header.set_type( PackageHeader::Publish);
	header.set_serialnum(0);
	header.set_srcaddr( getAddress());
	header.set_dstaddr(0);
	MessageUtil::serializePackageToString(header, appList, true, 50, &data);

	for (int i = 0; i < appList.addrs_size(); i++)
	{
		AppAddress dest = appList.addrs(i);
		SessionPtr connDest = sessionManager.getSession(dest);
		if (connDest)
			connDest->sendMsg(data);
	}
}

void BrokerServer::updateService( int serviceId)
{

	publishAppServer(serviceId);
}
#ifdef __linux__
typedef struct {
	unsigned long user;
	unsigned long nice;
	unsigned long system;
	unsigned long idle;
}Total_Cpu_Occupy_t;


typedef struct {
	unsigned int pid;
	unsigned long utime;  //user time  
	unsigned long stime;  //kernel time  
	unsigned long cutime; //all user time  
	unsigned long cstime; //all dead time  
}Proc_Cpu_Occupy_t;

//获取第N项开始的指针  
const char* get_items(const char*buffer ,unsigned int item){  

	const char *p =buffer;  

	int len = strlen(buffer);  
	int count = 0;  

	for (int i=0; i<len;i++){  
		if (' ' == *p){  
			count ++;  
			if(count == item -1){  
				p++;  
				break;  
			}  
		}  
		p++;  
	}  

	return p;  
}  

//获取总的CPU时间  
unsigned long get_cpu_total_occupy(){

	FILE *fd;  
	char buff[1024] = { 0 };
	Total_Cpu_Occupy_t t;

	fd = fopen("/proc/stat", "r");
	if (nullptr == fd){
		return 0;
	}

	fgets(buff, sizeof(buff), fd);
	char name[64] = { 0 };
	sscanf(buff, "%s %ld %ld %ld %ld", name, &t.user, &t.nice, &t.system, &t.idle);
	fclose(fd);

	return (t.user + t.nice + t.system + t.idle);
}


//获取进程的CPU时间  
unsigned long get_cpu_proc_occupy(unsigned int pid){

	char file_name[64] = { 0 };
	Proc_Cpu_Occupy_t t;
	FILE *fd;
	char line_buff[1024] = { 0 };
	sprintf(file_name, "/proc/%d/stat", pid);

	fd = fopen(file_name, "r");
	if (nullptr == fd){
		return 0;
	}

	fgets(line_buff, sizeof(line_buff), fd);

	sscanf(line_buff, "%u", &t.pid);
	const char *q = get_items(line_buff, PROCESS_ITEM);
	sscanf(q, "%ld %ld %ld %ld", &t.utime, &t.stime, &t.cutime, &t.cstime);
	fclose(fd);

	return (t.utime + t.stime + t.cutime + t.cstime);
}


//获取CPU占用率  
float get_proc_cpu(unsigned int pid){

	unsigned long totalcputime1, totalcputime2;
	unsigned long procputime1, procputime2;

	totalcputime1 = get_cpu_total_occupy();
	procputime1 = get_cpu_proc_occupy(pid);

	usleep(200000);

	totalcputime2 = get_cpu_total_occupy();
	procputime2 = get_cpu_proc_occupy(pid);

	float pcpu = 0.0;
	if (0 != totalcputime2 - totalcputime1){
		pcpu = 100.0 * (procputime2 - procputime1) / (totalcputime2 - totalcputime1);
	}

	return pcpu;
}
#endif
void BrokerServer::onTimer()
{
#ifdef __linux__
	prctl(PR_SET_NAME, "timer");
#endif
	while (true)
	{
		static int count = 0;
		count++;
		if (count % 5 == 0)
		{
			if (sconf.clustermode == enumClusterMode::MasterSlave)
			{
				//总线之间的心跳
				publishBrokerInfo();
			}
			//通知服务的主从状态
			noticeServer();
#ifdef __linux__
			pid_t pid = getpid();
			float p=get_proc_cpu(pid);
			int core=0;//get_nprocs();
			LOG4_INFO("CPU occupy:%d%,core:%d", (int)p, core);
			if (p > 20.0f)
			{
				char str[128];
				memset(str,0,sizeof(str));
				snprintf(str, 128, "CPU occupy:%d%", (int)p);
				client.ReportServerEvent(10, str);
			}
#endif
		}
		if (count % 30 == 0)
		{
			string serviceInfo = mServiceManager->getServicesInfo();
			LOG4_INFO("%s", serviceInfo.c_str());
			int count = sessionManager.getSessionCount();
			LOG4_INFO("Connection count:%d", count);
			char str[128];
			memset(str, 0, sizeof(str));
			snprintf(str, 128, "Connection count:%d", count);
			client.ReportServerEvent(10, str);
			client.ReportServerEvent(10, serviceInfo);
		}
		if (count % 300 == 0)
		{
			/*LOG4_INFO("Thread Info:");
			std::unordered_map<string, string>::const_iterator it = mapThread.begin();
			while (it != mapThread.end())
			{
				LOG4_INFO("%s", it->second.c_str());
				it++;
			}*/
		}
		
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}

#ifdef __linux__
void prog_exit(int signo)
{
	LOG4_INFO("signal:%d",signo);
}
#endif

int BrokerServer::acceptLoop(void* arg)
{
//#ifdef WIN32
//	evthread_use_windows_threads();//win上设置
//#else
//	//LOG4_ERROR("evthread_use_pthreads error!");
//	evthread_use_pthreads();    //unix上设置
//#endif
#ifdef __linux__
	signal(SIGCHLD, SIG_DFL);
	signal(SIGPIPE, SIG_IGN);
	signal(SIGKILL, prog_exit);
	signal(SIGTERM, prog_exit);
	signal(SIGSTOP, prog_exit);
#endif

	char tmp[64];
	memset(tmp, 0, sizeof(tmp));
	int64_t threadid = (int64_t)sp_thread_self();
	snprintf(tmp, 64, "0x%llx", (long long unsigned int)threadid);
#ifdef __linux__
	prctl(PR_SET_NAME, "accept");
#endif
	bool needkick = false;
	while (1)
	{
		LOG4_INFO("Begin event base work,id=%llx", threadid);
		const char ** methods = event_get_supported_methods();
		struct event_config *pCfg = event_config_new();
		event_config_avoid_method(pCfg, "evport");
		event_config_avoid_method(pCfg, "kqueue");
		// 分配并初始化event_base 
		base = event_base_new_with_config(pCfg);
		if (!base) {
			LOG4_ERROR("Could not initialize libevent!");
			return 1;
		}

		int ret = evthread_make_base_notifiable(base);

		// 初始化sockaddr_in结构体，监听在0.0.0.0:6610 
		struct sockaddr_in sin;
		memset(&sin, 0, sizeof(sin));
		sin.sin_family = AF_INET;
		sin.sin_port = htons(sconf.port_);
		// bind在上面制定的IP和端口，同时初始化listen的事件循环和callback：listener_cb 
		// 并把listener的事件循环注册在event_base：base上 
		listener = evconnlistener_new_bind(base, listener_cb, (void *)this,
			LEV_OPT_REUSEABLE | LEV_OPT_CLOSE_ON_FREE, -1,
			(struct sockaddr*)&sin,
			sizeof(sin));

		if (!listener) {
			LOG4_ERROR("Could not create a listener!");
			event_base_free(base);
			base = NULL;
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			continue;
		}
		LOG4_INFO("Broker listen on port:%d", sconf.port_);
		// 初始化信号处理event 
		signal_event = evsignal_new(base, SIGINT, signal_cb, (void *)this);
		if (!signal_event || event_add(signal_event, NULL) < 0) {
			LOG4_ERROR("Could not create/add a signal event!");
			return 1;
		}

		//struct timeval tv = { 3, 0 };
		//timeout_event = event_new(base, -1, 0, cycle_timout_cb, (void*)this);
		//if (!timeout_event)
		//{
		//	LOG4_ERROR("Could not create/add a timeout event!");
		//}
		//event_add(timeout_event, &tv);
		LOG4_INFO("Broker started!");
		// 程序将在下面这一行内启动event循环，只有在调用event_base_loopexit后 
		// 才会从下面这个函数返回，并向下执行各种清理函数，导致整个程序退出 
		//event_base_dispatch(base);
		if (needkick)
		{
			//踢所有连接
			kickAll();
			sessionManager.clearAll();
			needkick = false;
		}
		bool whileexit = false;
		int count = 0;
		time_t tm;
		time(&tm);
		while (1)
		{
			event_base_loop(base, EVLOOP_ONCE);
			//LOG4_INFO("event_base_loop");
			count++;
			time_t tm2;
			time(&tm2);
			int fre = 0;
			if (tm2 - tm >= 10)
			{
				fre = (count / (tm2 - tm));
				LOG4_INFO("event_base_loop frequency:%d", fre);
				tm = tm2;
				count = 0;
				if (whileexit)
					break;
			}
			if (fre > 1000)
			{
				char str[1024];
				memset(str, 0, sizeof(str));
				snprintf(str, 1024, "TPS=%d", fre);
				client.ReportServerEvent(10, str);
			}
			/*if (fre > 10000)
			{
				tm = tm2;
				whileexit = true;
				needkick = true;
				LOG4_ERROR("something wrong with event base.");
				string serviceInfo = mServiceManager->getServicesInfo();
				LOG4_INFO("%s", serviceInfo.c_str());
				int num = sessionManager.getSessionCount();
				LOG4_INFO("Connection count:%d", num);
				char str[1024];
				memset(str,0,sizeof(str));
				snprintf(str, 1024, "something wrong with event base,connect count=%d", num);
				client.ReportServerEvent(10, str);
			}*/
		}
		// 各种清理free 
		evconnlistener_free(listener);
		listener = NULL;
		event_free(signal_event);
		signal_event = NULL;
		event_base_free(base);
		base = NULL;
	}
	LOG4_INFO("Event base message loop exit");
	return 0;
}

void BrokerServer::handleOneEvent()
{
	//brokerServer::eventLoop(mEventArg, mAcceptArg);
}
void BrokerServer::sequenceTaskLoop(unsigned int index)
{
	char tmp[64];
	memset(tmp,0,sizeof(tmp));
	int64_t threadid = (int64_t)sp_thread_self();
	snprintf(tmp, 64, "0x%llx", (long long unsigned int) threadid);
	LOG4_INFO("Sequence task thread start,id=%llx,name=%s", threadid,tmp);
#ifdef __linux__
	prctl(PR_SET_NAME, tmp);
#endif
	std::unique_lock<std::mutex> lock(mutexThread);
	mapThread.insert(std::make_pair(tmp, tmp));
	lock.unlock();
	for (; 0 == mIsShutdown;) 
	{
		Task * task = (Task*)mSequenceTaskQueue[index].pop();
		if (task == NULL)
			continue;
		handleOneTask(task);
	}
}
void BrokerServer::unsequenceTaskLoop(unsigned int index)
{
	char tmp[64];
	memset(tmp,0,sizeof(tmp));
	int64_t threadid = (int64_t)sp_thread_self();
	snprintf(tmp, 64, "0x%llx", (long long unsigned int)threadid);
	LOG4_INFO("Unsequence task thread start,id=%llx,name=%s", threadid,tmp);
#ifdef __linux__
	prctl(PR_SET_NAME, tmp);
#endif
	std::unique_lock<std::mutex> lock(mutexThread);
	mapThread.insert(std::make_pair(tmp, tmp));
	lock.unlock();
	for (; 0 == mIsShutdown;)
	{
		Task * task = (Task*)mUnsequenceTaskQueue.pop();
		if (task == NULL)
			continue;
		handleOneTask(task);
	}
}
void BrokerServer::handleOneTask(Task* task)
{
	if (task == NULL)
		return;

	time_t time1;
	time(&time1);
	struct tm tm1;
#ifdef WIN32  
	tm1 = *localtime(&time1);  
#else  
	localtime_r(&time1, &tm1);
#endif 
	char tmp[128];
	int64_t threadid = (int64_t)sp_thread_self();
	char key[64];
	memset(key, 0, sizeof(key));
	snprintf(key, 64, "0x%llx", (long long unsigned int) threadid);
	if (task->request->classtype().size()>0)
		snprintf(tmp, 128, "0x%llx:[%2.2d:%2.2d:%2.2d]%s", (long long unsigned int) threadid, tm1.tm_hour, tm1.tm_min, tm1.tm_sec, task->request->classtype().c_str());
	else
		snprintf(tmp, 128, "0x%llx:[%2.2d:%2.2d:%2.2d]%d", (long long unsigned int) threadid, tm1.tm_hour, tm1.tm_min, tm1.tm_sec, task->request->GetCommand());
	mapThread[key] = tmp;

	if (task->request->classtype().compare("MsgExpress.HeartBeat") != 0)
	    LOG4_INFO("Process a task,src:%s,task:%s",  task->srcAddr.toString().c_str(),task->request->DebugString().c_str());
	SessionPtr conn = sessionManager.getSession(task->srcAddr);
	if (conn)
	{
		conn->handle(task);
		for (int i = 0; i < task->vecResponse.size(); i++)
		{
			for (int j = 0; j < task->vecResponse[i]->vecDestAddr.size(); j++)
			{
				AppAddress dest = task->vecResponse[i]->vecDestAddr[j];
				if (task->request->classtype().compare("MsgExpress.HeartBeat") != 0)
				    LOG4_INFO("Send msg to %s,msg=%s", dest.toString().c_str(), task->vecResponse[i]->header.DebugString().c_str());
				SessionPtr connDest = sessionManager.getSession(dest);
				if (connDest)
					connDest->sendMsg(task->vecResponse[i]->msg);
			}
		}
	}
	delete task;
}
void BrokerServer::handleOneComplete()
{

}


void BrokerServer::dispatch(const vector<shared_ptr<Response>>& vecMsg)
{
	for (int i = 0; i < vecMsg.size(); i++)
	{
		for (int j = 0; j < vecMsg[i]->vecDestAddr.size(); j++)
		{
			AppAddress dest = vecMsg[i]->vecDestAddr[j];
			SessionPtr connDest = sessionManager.getSession(dest);
			if (connDest)
				connDest->sendMsg(vecMsg[i]->msg);
		}
	}
}

void BrokerServer::pushTask(PackagePtr package, AppAddress src)
{
	Task* task = new Task();
	task->srcAddr = src;
	task->request = package; 
	if (package->header().issequence())
	{
		if (package->IsRequest())
		    mSequenceTaskQueue[src.AddressNo % mMaxThreads].push(task);
		else if (package->IsResponse())
			mSequenceTaskQueue[((AppAddress)package->GetDstAddr()).AddressNo % mMaxThreads].push(task);
		else if (package->IsPublish())
			mSequenceTaskQueue[(unsigned int)package->header().command() % mMaxThreads].push(task);
	}
	else
		executor.commit(bind(&BrokerServer::handleOneTask, this, std::placeholders::_1), task);
}

void BrokerServer::addSendTask(function<void(void*)> fun, void*arg)
{
	executor.commit(fun, arg);
	//sendThread.commit(fun, arg);
}

void BrokerServer::setThreadName()
{
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
	char tmp[64];
	memset(tmp, 0, sizeof(tmp));
	int64_t threadid = (int64_t)sp_thread_self();
	snprintf(tmp, 64, "0x%llx", (long long unsigned int)threadid);
	LOG4_INFO("task thread start,id=%llx,name=%s",threadid, tmp);
#ifdef __linux__
	prctl(PR_SET_NAME, tmp);
#endif
	std::unique_lock<std::mutex> lock(mutexThread);
	mapThread.insert(std::make_pair(tmp, tmp));
	lock.unlock();
}
int BrokerServer::start()
{
#ifdef __linux__
	//prctl(PR_SET_NAME, "broker");
#endif

	std::thread threanmain(bind(&BrokerServer::acceptLoop, this, std::placeholders::_1), this);
	threanmain.detach();

	sendThread.init(1);
	sendThread.commit(bind(&BrokerServer::onTimer, this), this);

	mSequenceTaskQueue = new SafeQueue[mMaxThreads];
	for (int i = 0; i < mMaxThreads; i++) {
		sequencePool.emplace_back(thread(bind(&BrokerServer::sequenceTaskLoop, this, std::placeholders::_1), i));
		sequencePool[i].detach();
	}

	executor.init(mMaxThreads);
	for (int i = 0; i < mMaxThreads; i++)
		executor.commit(bind(&BrokerServer::setThreadName, this));
	
	//client.Initialize();
	return 0;
}
