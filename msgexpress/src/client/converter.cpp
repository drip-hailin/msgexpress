#include "converter.h"

#define CALCVALUE(value,str) \
	str.resize(sizeof(value)); \
	memcpy((void*)(str.data()),&value,str.size());

string Converter::toString(int value)
{
	value=htonl(value);
	string str;
	CALCVALUE(value,str);
	return str;
}

string Converter::toString(unsigned int value)
{
	value=htonl(value);
	string str;
	CALCVALUE(value,str);
	return str;
}

string Converter::toString(int64_t value)
{
	value=htonll(value);
	string str;
	CALCVALUE(value,str);
	return str;
}

string Converter::toString(uint64_t value)
{
	value=htonll(value);
	string str;
	CALCVALUE(value,str);
	return str;
}

string Converter::toString(float value)
{
	string str;
	CALCVALUE(value,str);
	return str;
}

string Converter::toString(double value)
{
	string str;
	CALCVALUE(value,str);
	return str;
}

int Converter::toInt32(const string& value)
{
	return ntohl(*(int*)value.data());
}
unsigned int Converter::toUInt(const string& value)
{
	return (unsigned int)ntohl(*(unsigned int*)value.data());
}
int64_t Converter::toInt64(const string& value)
{
	return (int64_t)ntohll(*(int64_t*)value.data());
}
uint64_t Converter::toUInt64(const string& value)
{
	return (uint64_t)ntohll(*(uint64_t*)value.data());
}
float Converter::toFloat(const string& value)
{
	return *(float*)value.data();
}
double Converter::toDouble(const string& value)
{
	return *(double*)value.data();
}

