#pragma once

#include <errno.h>
#include <hash_map>
#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <atomic>

#include "../inc/package.h"
#include "../core/msgdecoder.h"
#include "../core/processinfo.h"
#include "../core/heap.h"
#include "syncmessagearray.h"
#include "../inc/service_config.h"
#include "../core/servicemanager.h"
#include "../core/connection.h"

#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/thread.h>
#include <event2/util.h>
#include <event2/event.h>

using namespace std::placeholders;

typedef  shared_ptr<MsgExpress::PublishData> PublishPtr;

#ifdef __linux__
#include <fcntl.h>
#include <sys/epoll.h>
#include <sys/time.h>
//#include "../pgm/PgmRecv.h"
//#include "../pgm/PgmSend.h"
#else

#endif

using namespace google::protobuf;
using namespace std;

struct SendDataItem
{
	string strData;
	SendDataItem()
	{
	}
	SendDataItem(const char* _data,unsigned int len )
	{
		strData.assign(_data,len);
	}
	const char* data()
	{
		return strData.data();
	}
	unsigned int size()
	{
		return strData.size();
	}
	~SendDataItem()
	{

	}
};

class MsgExpressProxy;
class  AppServerImpl
{
friend class MsgExpressProxy;
friend class GatewayProxy;
private:

#ifdef _WIN32
	struct IocpEvent {
			enum { eEventRecv, eEventSend };

			OVERLAPPED mOverlapped;
			WSABUF mWsaBuf;
			int mType;
	};

	struct ClientInfo {
			SOCKET mFd;

			IocpEvent mRecvEvent;
			IocpEvent mSendEvent;

			int mIsStop;
	};

	struct SP_TestStat {
			int mRecvFail;
			int mWSARecvFail;

			int mSendFail;
			int mWSASendFail;

			int mGQCSFail;
	};
#elif __linux__
	struct IocpEvent {
			enum { eEventRecv, eEventSend };

			int mType;
	};

	struct ClientInfo {
			SOCKET mFd;

			int mIsStop;
	};

	struct SP_TestStat {
			int mRecvFail;
			int mWSARecvFail;

			int mSendFail;
			int mWSASendFail;

			int mGQCSFail;
	};
#endif
	
private:
	atomic<unsigned short> syncnum_;//同步消息流水号
	atomic<unsigned short> asynnum_;//异步消息流水号

	int syncnum(){ int ret = ++syncnum_; return ret == 0xFFFF ? ++syncnum_ : ret; }
	int asynnum(){ int ret = ++asynnum_; return (ret == 0xFFFF ? ++asynnum_ : ret) + 0xFFFF; }
private:
	SyncMessageArray* m_pSyncMsgArr;
	SafeQueue *sendMsgQueue;
	SafeQueue completeMsgQueue;

	unsigned int mMaxThreads;

	struct event_base * base;
	struct evbuffer* databuff;
	Connection * conn;

	std::mutex mBufferMutex;
    MsgDecoder *decoder;
	int m_iHeartBeatTick;
	int m_iTryConnectTick;
	int m_iTryConnectInterval;
	ProcessInfo processInfo;
	MsgExpress::HeartBeat heartbeatInfo;
	std::mutex mHeartbeatMutex;

	std::mutex mutexThread;
	std::unordered_map<string, string> mapThread;

	AppServerImpl* logServer;
#ifdef _WIN32
	HANDLE hIocp;

#elif __linux__
	int epfd;
	bool needCreatEP;
	struct epoll_event ev, events[20];

	//PgmRecv *pgmRecv;
	//PgmSend *pgmSend;
#endif
	ClientInfo  clientInfo;
	bool isRun;
	std::mutex mCloseMutex;
	enum STATE{
		OFFLINE=0,
		ONLINE,
		LOGINNING,
		LOGINED,
	};
	int state;
	int serverMode;//Mater,Slave or Standby
	unsigned int address;
	//记录每次所获取的server IP和port的索引
	unsigned int serverIndex;
	bool heartbeatrunning;
	int minoff;
	int timeoff;//跟服务器端之间的时间差
	string serverStatus;//AppServer状态信息，通过心跳传递给broker
	std::mutex statusMutex;
private:
	static int identity;
	int id;
	time_t startTime;
	ClientCFG clientCfg;
	ProxyCFG proxyCfg;
	bool inited;
public:
	AppServerImpl();
	virtual ~AppServerImpl();
private:
	static sp_thread_result_t SP_THREAD_CALL  msgLoop(void* arg);
	static sp_thread_result_t SP_THREAD_CALL  sendThread(void* arg);
	static sp_thread_result_t SP_THREAD_CALL  completeThread(void* arg);
	static sp_thread_result_t SP_THREAD_CALL  eventThread(void* arg);
	static sp_thread_result_t SP_THREAD_CALL  timeoutThread(void* arg);
	static sp_thread_result_t SP_THREAD_CALL  heartBeatThread(void* arg);

	void processMsg(int index);
	void processSend();
	void processComplete();
	void processEvent();
	void processTimeout();
#ifdef _WIN32
	void on_read( ClientInfo * client, IocpEvent * event );
	void on_write( ClientInfo * client, IocpEvent * event );
#endif
	bool sendmsg(const  Message& msg, int cmd,unsigned int serialNum,Options options=Options() );
    bool postRawData(const char* data,unsigned int size);
	bool postRawData(SendDataItem* item);
	bool postRawData(const vector<SendDataItem*>& vecItems);
	bool directSend(SendDataItem* item);
	void Tongji(const char* data);
protected:

	std::mutex mutexFrequency;
	unordered_map<unsigned int,uint64_t> mapFrequency;

	unordered_set<int> mOrderedTopics;//有过滤条件
	unordered_set<int> mSimpleTopics;//没有过滤条件
	ServiceManager mSubscriptionManager;

	struct TimeoutItem
	{
		unsigned int msTimeout;
		unsigned int serial;
		timeval tv;
		bool operator < (const TimeoutItem& right) const
		{
			return (this->tv.tv_sec>right.tv.tv_sec) || (this->tv.tv_sec==right.tv.tv_sec && this->tv.tv_usec>right.tv.tv_usec);
		}
	};
	struct MsgCallbackInfo
	{
		function<void(MsgParams&)> cb;
		MsgParams params;
	};
	typedef  shared_ptr<MsgCallbackInfo> MsgCallbackPtr;
	unordered_map<unsigned int,MsgCallbackPtr> mMapAsyncInfo;//
	std::mutex cbMutex;
	MyHeap<TimeoutItem> minheap;
	std::mutex mMutex;
	sp_thread_semaphore_t timeoutSem;

	unordered_map<unsigned int,shared_ptr<std::string>> mMapMultiPackage;//
	std::mutex mMutexMultiPackage;

	TimeoutItem createTimeout(unsigned int timeval,int serial);
	DWORD timeoutNext( MyHeap<TimeoutItem>& timeoutHeap );
	void  popAndCallback(  );
	MsgCallbackPtr getCallbackInfo(unsigned int serialNum,bool erase=true);
	void clearTimeout();
public:
	///@brief 客户端初始化函数,
	int  Initialize(const char* configPath=NULL,ProxyCFG* proxyCfg=NULL);
	int  Initialize(const wchar_t* configPath,ProxyCFG* proxyCfg=NULL);
    int  Initialize(ClientCFG* pClientCfg,ProxyCFG* pProxyCfg);
	///@brief 客户端登录Data Bus是否成功
	bool IsLogin();
    ///@brief 客户端释放函数,
    bool  Release();
	///@brief 客户端发送异步消息的接口,返回流水号
	unsigned int  PostMessage(const Message& msg,int appId=-1,unsigned int dstaddr=0);
	///@brief 客户端发送应答消息的接口
	bool Reply(PackagePtr requestPackage,const Message& replyMsg);
	bool Reply(PackagePtr requestPackage,unsigned int errCode,string errMsg);
	///@brief 客户端发送同步消息的接口
	bool SendMessage(const Message& request, MessagePtr& response,unsigned int milliseconds,Options options=Options());
	//订阅消息
	bool Subscribe(const MsgExpress::SubscribeData& subData, MessagePtr& response);
	bool UnSubscribe(const MsgExpress::UnSubscribeData& subData, MessagePtr& response);
	bool ComplexSubscribe(const MsgExpress::ComplexSubscribeData& subData, MessagePtr& response);
	//简单订阅
	bool Subscribe(int subid,const google::protobuf::Message& subData, MessagePtr& response,AppAddress addr);
	bool Subscribe(int subid, const vector<string>& topics, MessagePtr& response, AppAddress addr);
	bool Subscribe(int subid, const vector<MessagePtr>& vecSubmsg, MessagePtr& response, AppAddress addr);
	bool UnSubscribe(int subid, MessagePtr& response);
	//推送消息
	bool Publish(MessagePtr msg,function<void(const MsgParams&)> msgcb,void* arg,unsigned int msTimeout=0);
	MsgExpress::ErrMessage GetLatestError();
	void ClearLatestError();
	unsigned int GetAddress(){ return address; }

	uint64_t GetBrokerTime();//单位毫秒
	int GetQueueSize(){ return decoder->getQueueSize();  }
	ClientCFG GetConfig(){ return clientCfg; }
	void ReportServerEvent(int level, const string& status);

	unsigned int  PostMessage(int cmd,const char* buff,unsigned int size,Options options=Options());
	bool PostMessage(MessagePtr msg,function<void(const MsgParams&)> msgcb,void* arg,unsigned int msTimeout=0,Options options=Options());
	bool SendMessage(unsigned int cmd,const char* buff,unsigned int size,PackageHeader& header,string& retData,unsigned int milliseconds,unsigned int dstaddr=0);
	bool Reply(const PackageHeader& header,const char* buff,unsigned int size);
private://
	SOCKET ConnectServer(const char* strAddr, unsigned short nHostPort);
	void CloseSocket(SOCKET& sock);
private:
	///@brief 客户端连接Data Bus
	int Connect(ClientCFG* pClientCfg,ProxyCFG* pProxyCfg);
	///@brief 客户端释关闭连接
	void Close();
	///@brief 客户端登录Data Bus
	int Login();
	///@brief注册appserver。获取地址
	int RegAppServer();
	///@brief注册服务
	int RegService();
	///@brief注册订阅信息
	int RegSubscribe();
	///@brief 初始化相关处理线程
	int InitThread();
	///@brief 客户端是否onLine
	bool IsOnLine();
	///@brief 发送一个心跳包
	void SendHeartBeat();
	///@brief 接收到一个心跳反馈包
	void OnHeartBeatMessage(PackagePtr);
	///@brief 自动重连Data Bus Server
	int AutoTryConnectDataBusServer();
	void HeartBeatCallback(const MsgParams& params);
	void LoginCallback(const MsgParams& params);
	void SubscribeCallback(const MsgParams& params);
	//订阅消息
	bool Subscribe2Broker(const MsgExpress::SubscribeData& subData, MessagePtr& response);

	void HaveASleep(DWORD milliseconds);
	static MsgExpress::ErrMessage* GetErrorMsg();
protected:
	void LogCallback(string level,string log);
    MessagePtr CreateMessage(const string& clazz);
	bool ProcessMultiPackage(PackagePtr package);
        bool ParseMessage(PackagePtr package,string& errstr);
	bool ParseMessage(PackagePtr package);
	virtual void OnRawPackage(PackagePtr package);
	virtual void OnMessage(PackagePtr package);
	virtual void OnPublish(PackagePtr package);
	virtual void OnEvent(int eventId);
public:
	function<MessagePtr(const std::string&)> onCreateMessage;
	function<void(const char* buf, unsigned int size)> onRawData;
	function<void(PackagePtr)> onMessage;
	function<void(PackagePtr)> onPublish;
	function<void(int)> onEvent;
	function<void()> onConnect;
	function<void()> onClose;

	function<void(PackagePtr)> onRequest;

};

