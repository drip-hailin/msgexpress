#include "proxyclient_config.h"
#include "../inc/logger.h"
#include <iostream>

bool ProxyClientConfig::ReadConfigFile()
{
	TiXmlElement *root = mDocument->FirstChildElement("configuration");
	if(!root)
		return false;
    if (!ReadProxyClientCFG(root))
        return false;
    return true;
}
bool ProxyClientConfig::ReadProxyClientCFG(TiXmlElement *root)
{
	TiXmlElement *redis = root->FirstChildElement("cache");
	if( redis != NULL)
	{
		//const char* str=redis->Attribute("port",&storage_port);
		const char* str=redis->Attribute("db",&storage_db);
		//const char* addr=redis->Attribute("ip");
		//if(addr)
		//	storage_ip=addr;
		LOG4_INFO( "Proxy storage settings,ip=%s,port=%d,db=%d" ,storage_ip.c_str(),storage_port,storage_db );
	}
	TiXmlElement *pparent = root->FirstChildElement("clientA");
	if( pparent == NULL)
		return false;
	if(ReadServerCFG(pparent,clientCfg) == false)
        return false;
	pparent = root->FirstChildElement("clientB");
	if( pparent == NULL)
		return false;
	return ReadServerCFG(pparent,clientCfgB);
}



