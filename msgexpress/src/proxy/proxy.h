#ifndef _MSGEXPRESSPROXY_H_
#define _MSGEXPRESSPROXY_H_

#include <atomic>
#include <hash_map>
#include <unordered_map>
#include <time.h>
#include "../client/client_impl.h"
#include <functional>
using namespace std;

#ifdef __GNUC__
using namespace __gnu_cxx;
#ifndef HASDEFHASHSTRING
#define HASDEFHASHSTRING
namespace __gnu_cxx
{
    template<> struct hash<string>
    {
	size_t operator()(const string& s) const
	{
	    return __stl_hash_string(s.c_str());
	}
    };
}
#endif
#endif

class ProxyClientConfig;

class ProxyClient : public AppServerImpl
{
public:
	ProxyClient();
	virtual ~ProxyClient();
protected:
	virtual void OnRawPackage(PackagePtr package);
	virtual void OnEvent(int eventId);
public:
	function<void(PackagePtr)> funOnRawPackage;
    function<void(int)> funOnEvent;
    
};

class MSGEXPRESS_API MsgExpressProxy
{
private:
    struct ItemInfo
	{
        unsigned int serialNum;
        unsigned int srcAddr;
		uint64_t sendtime;
	};
	atomic<unsigned int> serialnum;//��Ϣ��ˮ��
	typedef shared_ptr<ItemInfo> ItemInfoPtr;
    hash_map<unsigned int,ItemInfoPtr> mapInfoA;
    hash_map<unsigned int,ItemInfoPtr> mapInfoB;
	std::mutex mMutexA;
	std::mutex mMutexB;
public:
	MsgExpressProxy();
	virtual ~MsgExpressProxy();
public:
	bool Start(const char* configFile);
	virtual void Init(const ProxyClientConfig &pclientconf);
	void Print();
protected:
	ProxyClient* m_pServerA;
	ProxyClient* m_pServerB;
protected:
	void Exchange(PackagePtr package, AppServerImpl* pServer, hash_map<unsigned int, ItemInfoPtr>& mapA, hash_map<unsigned int, ItemInfoPtr>& mapB, std::mutex& mutexA, std::mutex& mutexB);

	virtual void OnRawDataA(const char* buf,unsigned int size);
	virtual void OnRawPackageA(PackagePtr package);
	virtual void OnEventA(int eventId);

	virtual void OnRawPackageB(PackagePtr package);
	virtual void OnEventB(int eventId);

};

#endif
