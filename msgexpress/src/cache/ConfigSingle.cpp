#include "ConfigSingle.h"
#include "../tinyxml/tinyxml.h"


CConfigSingle::CConfigSingle(void)
{
	this->LoadConfig();
}


CConfigSingle::~CConfigSingle(void)
{

}

CConfigSingle* CConfigSingle::GetInstance()
{
	static CConfigSingle instance;
	return &instance;
}

bool CConfigSingle::LoadConfig()
{
	string filePath = "../config/dbconfig.xml";
	TiXmlDocument *doc = new TiXmlDocument(filePath.c_str());

	if (!doc->LoadFile())
	{
		printf("load config fail!\n");
		exit(0);
	}
	
	TiXmlElement *root = doc->RootElement();
	host = root->FirstChildElement("host")->GetText();
	username = root->FirstChildElement("username")->GetText();
	password = root->FirstChildElement("password")->GetText();
	dbName = root->FirstChildElement("dbName")->GetText();	


	return true;
}
