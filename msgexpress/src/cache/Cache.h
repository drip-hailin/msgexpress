#ifndef _SPCLIENT_TEST_H_
#define _SPCLIENT_TEST_H_


#include "../inc/msgexpress.h"
#include "../inc/clientbase.h"
#include "../protobuf/protobuf.h"
#include "../client/cacheapi.h"

using google::protobuf::Message;

class Cache:public DataBusClient
{
public:
	Cache();
	virtual ~Cache();
private:
	bool connected;
	unsigned char database;
public:
	int  Initialize(string addr,unsigned short port);
	int  Initialize(const char* configPath=NULL);
	bool IsConnected(){ return connected; }
	int ClearDB(string* errstr=0);
	int Select(unsigned char db,string* errstr=0);
	int Set(string key,string value,string* errstr=0);
	int Get(string key,string& value,string* errstr=0);
	int Del(std::string key,std::string* errstr=0);
	int SaveData(const Storage::SaveDataRequest& request,Storage::SaveDataResponse& resp,string* errstr=0);
	int QueryData(const Storage::QueryDataRequest& request,Storage::QueryDataResponse& resp,std::string* errstr=0);

	void Set(unsigned char dataset,const string& key,const string& value,void* userdata, std::function<void(const CallbackArg&)> cb);
	void Get(unsigned char dataset,const string& key,void* userdata, std::function<void(const CallbackArg&)> cb);
private:
	virtual void OnMessage(PackagePtr msg);
	virtual void OnPublish(PackagePtr data);
	virtual void OnEvent(int eventId);
};
#endif
