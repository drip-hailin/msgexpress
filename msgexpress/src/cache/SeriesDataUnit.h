#pragma once

#include <stdio.h>
#include <assert.h>

#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <list>
#include <unordered_map>
#include <queue>
#include "DataUnit.h"
#include "../core/spthread.h"

class SeriesDataUnit : public DataUnit
{
	struct DataSegment{
		uint64 from;
		uint64 to;
		std::vector<string> vecDataIndex;
	};
private:
	string unitkey;
	int topic;
	string tablename;
	//vector<int> keylist;
	sp_thread_mutex_t mutex;
	uint64 maxId;
	std::unordered_map<string,PublishPtr> mapDataItem;//<uuid,item>
	std::unordered_map<uint64,PublishPtr> mapIdItem;//<id,item>
	std::queue<string> queueUnsaved;//uuid
public:
	SeriesDataUnit(string unitkey,int topic,string tbl);
	virtual ~SeriesDataUnit(void);

	virtual uint64 PutData(PublishPtr data,bool loaddb,string& err);
	virtual void SyncData(PublishPtr data,vector<uint64>& vecLost);
	virtual uint32 QueryData(Storage::QueryDataRequest*,vector<PublishPtr>& dataset);
	virtual uint32 GetAllData(vector<PublishPtr>& dataset);
	virtual int32 DeleteData(Storage::DeleteDataRequest*,bool deleteDB);
	virtual void SaveDB();
	virtual bool HasUnSavedData(){ return queueUnsaved.size()>0; }
	virtual void Print();
	virtual string GetKey(){ return unitkey;}
private:
	void LoadData();

};

