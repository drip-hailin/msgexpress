// infoserver配置信息加载类
#ifndef _CONFIG_SINGLE_H_
#define _CONFIG_SINGLE_H_
#include <string>
#include <vector>

using std::string;
using std::vector;

class CConfigSingle
{
private:
	CConfigSingle(void);
	~CConfigSingle(void);
	CConfigSingle(const CConfigSingle& cs);
	CConfigSingle& operator=(const CConfigSingle& cs);

public:
	static CConfigSingle* GetInstance();

protected:
	// 加载配置项
	bool LoadConfig();

public:
	string host;                           // QM的MYSQL地址
	string username;
	string password;
	string dbName;	

};

#endif