#ifndef DBMYSQL_H_
#define DBMYSQL_H_
#ifdef _WIN32	
	#include <WinSock2.h>
#else
	#include <unistd.h>
#endif

#include <string>
#include <vector>


/****************************************************
//数据库操作封装类
*****************************************************/
#ifdef _WIN32
	#ifdef _M_X64
	#define MYSQLSUPPORT 1
	#endif
#elif __linux__
    #define MYSQLSUPPORT 1
#endif

#ifdef MYSQLSUPPORT
using std::string;
using std::vector;
namespace Database
{

class CRecordSet;
class DBMysql
{
public:
	DBMysql();
	~DBMysql(void);

	static DBMysql* GetInstance();                 // 文件消息连接
	static DBMysql* GetReadInstance();             // 读操作连接
	static DBMysql* GetWriteInstance();            // 写操作连接

	static DBMysql* GetInstanceOther();            // QB操作连接
	static DBMysql* GetTransactionInstance();      // 事物连接

	//设置连接选项
	bool SetOpion(enum mysql_option option, const string& arg);
	//初始化数据库连接
	bool ConnectDB(const string& host, const string& username, const string& password, const string& dbName, 
		unsigned int port = 3306, unsigned long client_flag = 0);
	//
	bool CanWork();
	//执行SQL语句
	bool ExecuteSql(const string& strSql);
	//开始事务 
	bool StartTransaction();
	//提交事务
	bool Commit();
	//回滚事务 
	bool Rollback();
	//选择数据库
	bool SelectDB(const string& dbName);
	//关闭数据库连接
	void CloseDB(void);
	//获得查询结果
	bool GetRecordSet(CRecordSet& rs);
	//返回错误信息
	string GetErrorInfo();
	//返回错误消息码
	size_t GetErrorNo();
	//执行SQL语句，并返回查询结果
	bool QueryAndGetResult(const string& strSql, CRecordSet& rs);
	//获取最后一次插入记录ID，失败返回0
	size_t GetLastInsertId();
	//数据库加锁
	bool LockTable(const string& tableName, const string& priority);
	//数据库解锁
	bool UnLockTable();
	//执行SQL语句，并判断查询结果是否为空
	bool IsQueryResultEmpty(const string& strSql);
	// 获取二进制数据 需要手动删除返回指针
	char* GetConvertBytesString(const string& str);

private:
	//确认SQL语句合法
	bool IsValid(const string& strSql);
	DBMysql(const DBMysql& sql);
	DBMysql& operator=(const DBMysql& sql);
	
private:	
	MYSQL mysql;
	bool bConnect;
	bool bReadConnect;
	bool bWriteConnect;
	bool bConnectOther;
	bool bTransactionConnect;
	CMutexLocker mutex;
	static CMutexLocker tMutex;
	static CMutexLocker tReadMutex;
	static CMutexLocker tWriteMutex;
	static CMutexLocker tMutexOther;
	static CMutexLocker tTransactionMutex;
};

class CRecordSet
{
public:
	CRecordSet();
	~CRecordSet();

	//插入每行记录
	void InsertData(const vector<string>& vec);
	//设置列数
	void SetColumnNum(size_t col);
	//设置行数
	void SetRowNum(size_t row);
	//获取列数
	size_t GetColumnNum() const;
	//获取行数
	size_t GetRowNum() const;
	//清空vector数据
	void Clear();
	//获取字段属性
	string GetDataAt(size_t row, size_t col) const;
	//插入列名
	void InsertFieldName(string fieldName);
	//输入列名获取列索引号
	int GetColumnIndex(const string& fieldName);
	//获取列名
	string GetFieldName(size_t col) const;
	//是否为空
	bool IsEmpty();

private:
	vector<string> fieldNameVec;
	vector<vector<string> > dataVec;
	size_t columnNum;
	size_t rowNum;
};

}
#endif

#endif

