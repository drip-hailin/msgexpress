
#include "DataManager.h"
#include <math.h>
#include "DBMysql.h"
#include <functional>
#include <unordered_set>
#include <algorithm>
#include <thread>
#include "../client/publish_inft.h"

#ifdef _WIN32
using namespace std::tr1::placeholders;
#elif __linux__
using namespace std::placeholders;
#endif

IDataManager* CreateDataManager()
{
	return new DataManager();
}
void FreeDataManager(IDataManager* manager)
{
	delete manager;
}

vector<string> split2(const string& src, string delimit, string null_subst="")  
{     
    vector<string> v;  
	if( src.empty() || delimit.empty() ) 
        return v; 
    size_t deli_len = delimit.size();  
    long index = -1, last_search_position = 0;  
    while( (index=src.find(delimit,     
        last_search_position))>-1 )  
    {  
        if(index==last_search_position)  
            v.push_back(null_subst);  
        else  
            v.push_back( src.substr(last_search_position, index-   
            last_search_position) );  
        last_search_position = index + deli_len;  
    }  
    string last_one = src.substr(last_search_position);  
    v.push_back( last_one.empty()? null_subst:last_one );  
    return v;  
}   
DataManager::DataManager(){ 
	sp_thread_mutex_init(&mutex,NULL);
	sp_thread_semaphore_init(&semph,0,1000);

	run=true;
	sp_thread_t thread;
	int ret1 = sp_thread_create( &thread, NULL, saveDBThread, this );
	if( 0 == ret1 ) {
		LOG4_INFO( "Thread #%ld has been created to process database", thread );
	} else {
		LOG4_ERROR( "Unable to create a thread to process database, %d",errno );
	}

	ret1 = sp_thread_create( &thread, NULL, printThread, this );
	if( 0 == ret1 ) {
		LOG4_INFO( "Thread #%ld has been created to print infomation", thread );
	} else {
		LOG4_ERROR( "Unable to create a thread to print infomation, %d",errno );
	}
}
DataManager::~DataManager()
{
	sp_thread_mutex_destroy(&mutex);
	sp_thread_semaphore_destroy(&semph);
}
sp_thread_result_t SP_THREAD_CALL  DataManager::saveDBThread(void* arg)
{
	DataManager* appServer=(DataManager*)arg;
	while(appServer->run)
	{
		appServer->processDB();
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	return 0;
}
sp_thread_result_t SP_THREAD_CALL  DataManager::printThread(void* arg)
{
	DataManager* appServer=(DataManager*)arg;
	while(appServer->run)
	{
		appServer->print();
		std::this_thread::sleep_for(std::chrono::milliseconds(60000));
	}
	return 0;
}

void DataManager::GetSetInfo(Storage::SetInfoRequest* req,Storage::SetInfoResponse* resp)
{
	for(int i=0;i<req->setid().size();i++)
	{
		int setid=req->setid(i);
		sp_thread_mutex_lock(&mutex);
		if(mapSetInfo.find(setid)==mapSetInfo.end())
		{
			LOG4_ERROR( "Set info not found,setid=%d" ,setid );
			sp_thread_mutex_unlock(&mutex);
			continue;
		}
	    SetInfo* info=mapSetInfo[setid];
		sp_thread_mutex_unlock(&mutex);
		Storage::SetInfoResponse_SetInfo* tpinfo=resp->add_setinfo();
		tpinfo->set_setid(setid);
		tpinfo->set_tablename(info->tablename);
		tpinfo->set_type(info->type);
		if(info->type!=SetInfo::Snapshot)
			continue;
		MsgExpress::PublishData pub;
		pub.set_topic(0);
		DataUnit* unit=GetUnit(setid,pub);
		if(unit)
		{
			vector<PublishPtr> dataset;
			unit->GetAllData(dataset);
			vector<PublishPtr>::const_iterator it=dataset.begin();
			while(it!=dataset.end())
			{
				MsgExpress::PublishData* data=tpinfo->add_data();
				data->CopyFrom(*it->get());
				it++;
			}
		}
	}
	//LOG4_INFO( "Finish GetTopicInfo" );
}
void DataManager::SetSetInfo(Storage::SetInfoResponse* resp)
{
	//LOG4_DEBUG( "Set topic info:%s" ,resp->ShortDebugString().c_str() );
	
	for(int i=0;i<resp->setinfo_size();i++)
	{
		const Storage::SetInfoResponse_SetInfo& tpinfo=resp->setinfo(i);
		int setid=tpinfo.setid();
		sp_thread_mutex_lock(&mutex);
		if(mapSetInfo.find(setid)!=mapSetInfo.end())
		{
			LOG4_ERROR( "Set info  exists,setid=%d" ,setid );
			sp_thread_mutex_unlock(&mutex);
			continue;
		}
		SetInfo* info=new SetInfo();
	    mapSetInfo[setid]=info;
		info->setid=setid;
		info->tablename=tpinfo.tablename();
		info->type=tpinfo.type();
		sp_thread_mutex_unlock(&mutex);
		MsgExpress::PublishData pub;
		pub.set_topic(0);
		DataUnit* unit=GetUnit(setid,pub);
		if(unit)
		{
			RepeatedPtrField<MsgExpress::PublishData>::const_iterator it=tpinfo.data().begin();
			while(it!=tpinfo.data().end())
			{
				vector<uint64> vecLost;
				PublishPtr data=(PublishPtr)(new MsgExpress::PublishData(*it));
				unit->SyncData(data,vecLost);
				it++;
			}
		}
	}
	
}

void DataManager::LoadDB()
{
#ifdef MYSQLSUPPORT
	sp_thread_mutex_lock(&mutex);
	Database::CRecordSet result;
	string sql="select * from topic order by idtopic";
	bool ret=Database::DBMysql::GetInstance()->QueryAndGetResult(sql,result);
	if(!ret){
		LOG4_ERROR( "Read table topic failed" );
		sp_thread_mutex_unlock(&mutex);
		return;
	}
	for(int row=0;row<result.GetRowNum();row++)
	{
		SetInfo* info=new SetInfo();
		string data=result.GetDataAt(row,0);
		int setid=atoi(data.c_str());
		mapSetInfo[setid]=info;
		data=result.GetDataAt(row,1);
		vector<string> keylist=split2(data,",");
		for(int i=0;i<keylist.size();i++)
		{
			info->keylist.push_back(atoi(keylist.at(i).c_str()));
		}
		data=result.GetDataAt(row,2);
		vector<string> collist=split2(data,",");
		for(int i=0;i<collist.size();i++)
		{
			info->collist.push_back(atoi(collist.at(i).c_str()));
		}
		info->tablename=result.GetDataAt(row,3);
		data=result.GetDataAt(row,4);
		info->type=atoi(data.c_str());
		info->setid=setid;
	}
	sp_thread_mutex_unlock(&mutex);
	std::unordered_map<int,SetInfo*>::const_iterator it=mapSetInfo.begin();
	while(it!=mapSetInfo.end())
	{
		int setid=it->first;
		char buf[128];
		if(it->second->type==SetInfo::Snapshot)
		    strprintf(buf,128,"select id,content from %s order by createtime",it->second->tablename.c_str());
		else
			strprintf(buf,128,"select id,content,uuid from %s order by createtime",it->second->tablename.c_str());
		sql=buf;
		result.Clear();
		ret=Database::DBMysql::GetInstance()->QueryAndGetResult(sql,result);
		if(ret){
			for(int row=0;row<result.GetRowNum();row++)
			{
				string strid=result.GetDataAt(row,0);
				uint64 id=atoi(strid.c_str());
				string content=result.GetDataAt(row,1);
				string uuid;
				if(it->second->type==SetInfo::TimeSeries)
				    uuid=result.GetDataAt(row,2);
				MsgExpress::PublishData* data=new MsgExpress::PublishData();
				if(!data->ParseFromString(content))
				{
					LOG4_ERROR( "Parse SaveDataRequest failed,uuid:%s,id=%s",uuid.c_str(),strid.c_str() );
					delete data;
				}
				else
				{
					string err;
					DataUnit* unit=GetUnit(setid,*data);
					if(unit)
					    unit->PutData(PublishPtr(data),true,err);
					else
						LOG4_ERROR( "Cant find data unit,uuid:%s,id=%s",uuid.c_str(),strid.c_str() );
				}
			}
		}
		it++;
	}
#endif
}
void DataManager::processDB()
{
#ifdef MYSQLSUPPORT
	sp_thread_semaphore_timedwait(&semph,10000);
	while(true)
	{
		string unitkey;
		DataUnit* unit=NULL;
		sp_thread_mutex_lock(&mutex);
		std::unordered_map<string,DataUnit*>::const_iterator it=mapUnSaved.begin();
		if(it!=mapUnSaved.end())
		{
			unitkey=it->first;
			unit=it->second;
		}
		sp_thread_mutex_unlock(&mutex);
		if(unit==NULL)
			break;
		//LOG4_ERROR( "processDB" );
		//check database connect status
		if(!Database::DBMysql::GetInstance()->CanWork())
		{
			LOG4_WARN( "Error connection status to database." );
			sp_sleep(2000);
			continue;
		}
		unit->SaveDB();
		sp_thread_mutex_lock(&mutex);
		if(!unit->HasUnSavedData())
			mapUnSaved.erase(mapUnSaved.find(unitkey));
		sp_thread_mutex_unlock(&mutex);
	}
#endif
}
void DataManager::print()
{
	sp_thread_mutex_lock(&mutex);
	std::unordered_map<string,DataUnit*>::const_iterator it=mapData.begin();
	while(it!=mapData.end())
	{
		it->second->Print();
		it++;
	}
	sp_thread_mutex_unlock(&mutex);
}
string DataManager::CreateKey(int setid,int topic,const RepeatedPtrField<MsgExpress::DataItem>& datalist,bool isQuery)
{
	if(mapSetInfo.find(setid)==mapSetInfo.end())
		return "";
	SetInfo* info=mapSetInfo[setid];
	string key;
#define BUFSIZE 64
	char buf[BUFSIZE];
	strprintf(buf,BUFSIZE,"%d",setid);
	key.append(buf);
	if(info->type==SetInfo::Snapshot)
	{
		return key;
	}
	strprintf(buf,BUFSIZE,"&t=%d",topic);
	key.append(buf);
	RepeatedPtrField<MsgExpress::DataItem>::const_iterator it=datalist.begin();
	while(it!=datalist.end())
	{
		bool ok=isQuery || it->ispk();
		if(ok)
		{
			if(it->ival_size()>0 )
				strprintf(buf,BUFSIZE,"&%d=%d",it->key(),it->ival(0));
			else if(it->uival_size()>0)
				strprintf(buf,BUFSIZE,"&%d=%d",it->key(),it->uival(0));
			else if(it->lval_size()>0)
				strprintf(buf,BUFSIZE,"&%d=%lld",it->key(),it->lval(0));
			else if(it->ulval_size()>0)
				strprintf(buf,BUFSIZE,"&%d=%lld",it->key(),it->ulval(0));
			else if(it->bval_size()>0)
				strprintf(buf,BUFSIZE,"&%d=%d",it->key(),it->bval(0)?1:0);
			else if(it->fval_size()>0)
				strprintf(buf,BUFSIZE,"&%d=%f",it->key(),it->fval(0));
			else if(it->dval_size()>0)
				strprintf(buf,BUFSIZE,"&%d=%d",it->key(),it->dval(0));
			else if(it->tval_size()>0)
				strprintf(buf,BUFSIZE,"&%d=%lld",it->key(),it->tval(0));
			else if(it->strval_size()>0)
				strprintf(buf,BUFSIZE,"&%d=%s",it->key(),it->strval(0).c_str());
			else
				ok=false;
			if(ok)
			    key.append(buf);
		}
		it++;
	}
	return key;
}
DataUnit* DataManager::GetUnit(int setid,const MsgExpress::PublishData& savedata)
{
	string unitkey=CreateKey(setid,savedata.topic(),savedata.item(),false);
	if(unitkey.size()<1)
		return NULL;
	string uuid=savedata.uuid();
	int topic=savedata.topic();
	if(mapSetInfo.find(setid)==mapSetInfo.end())
		return NULL;
	SetInfo* info=mapSetInfo[setid];
	DataUnit* unit=NULL;
	sp_thread_mutex_lock(&mutex);
	if(info->type==SetInfo::TimeSeries)
	{
		if(mapData.find(unitkey)!=mapData.end())
			unit=mapData[unitkey];
		else
		{
			unit=new SeriesDataUnit(unitkey,setid,info->tablename);
			mapData[unitkey]=unit;
		}
	}
	else if(info->type==SetInfo::Snapshot)
	{
		if(mapData.find(unitkey)!=mapData.end())
			unit=mapData[unitkey];
		else
		{
			unit=new SnapshotDataUnit(unitkey,setid,info->tablename);
			mapData[unitkey]=unit;
		}
	}
	sp_thread_mutex_unlock(&mutex);
	return unit;
}

int DataManager::PutData(Storage::SaveDataRequest* savedata,string& err,uint64& id)
{
	string uuid=savedata->content().uuid();
	int setid=savedata->setid();
	int topic=savedata->content().topic();
	id=0;
	int result=-1;
	if(mapSetInfo.find(setid)==mapSetInfo.end())
	{
		err="Set infomation not config";
		LOG4_ERROR("Set infomation not config,setid=%d",setid);
		return result;
	}
	DataUnit* unit=GetUnit(savedata->setid(),savedata->content());
	if(unit)
	{
		PublishPtr data=PublishPtr(new MsgExpress::PublishData(savedata->content()));
		id=unit->PutData(data,false,err);
		if(id>0)
		{
			sp_thread_mutex_lock(&mutex);
			mapUnSaved[unit->GetKey()]=unit;
	        sp_thread_mutex_unlock(&mutex);
			sp_thread_semaphore_post(&semph);
			result=0;
		}
		else
		{
			LOG4_ERROR("Putdata failed,err=%s,",err.c_str());
		}
	}
	else
	{
		err="Dataset not exits.";
		LOG4_ERROR("Dataset not exits.setid=%d",setid);
	}	
	return result;
}
int DataManager::DelData(Storage::DeleteDataRequest* req,string& err)
{
	int result=-1;
	string unitkey=CreateKey(req->setid(),req->topic(),req->condition(),true);
	DataUnit* unit=NULL;
	sp_thread_mutex_lock(&mutex);
	if(mapData.find(unitkey)!=mapData.end())
		unit=mapData[unitkey];
	sp_thread_mutex_unlock(&mutex);
	Storage::DeleteDataResponse* resp=new Storage::DeleteDataResponse();
	if(!unit)
	{
		result=-1;
		err="Dataset not exits.";
	}
	else
	{
		result=unit->DeleteData(req,true);
		if(result==0)
		{
			sp_thread_mutex_lock(&mutex);
	        mapUnSaved[unitkey]=unit;
	        sp_thread_mutex_unlock(&mutex);
			sp_thread_semaphore_post(&semph);
		}
		else
			err="delete data failed";
	}
	return result;
}
int DataManager::QueryData(Storage::QueryDataRequest* req,string& err,vector<PublishPtr>& dataset)
{
	int result=-1;
	//uint64 from=req->from();
	//uint64 to=req->to();
	//uint32 num=req->num();
	string unitkey=CreateKey(req->setid(),req->topic(),req->condition(),true);
	DataUnit* unit=NULL;
	sp_thread_mutex_lock(&mutex);
	if(mapData.find(unitkey)!=mapData.end())
		unit=mapData[unitkey];
	sp_thread_mutex_unlock(&mutex);
	if(!unit)
	{
		result=-1;
		err="Dataset not exits.";
	}
	else
	{
		result=0;
		uint32 total=unit->QueryData(req,dataset);
		if(dataset.size()<1)
		{
			result=-1;
			err="Key not exists";
		}
	}
	return result;
}

void DataManager::SyncData(PublishPtr pub)
{
	if(pub->topic()==Storage::TOPIC_NEWDATA)
	{
		PublishHelperPtr helper=CreatePublishHelper(pub.get(),true);
		const MsgExpress::DataItem * item=helper->GetItem(Storage::KEY_DATASET);
		if(!item || item->ival_size()<1)
			return;
		int setid=item->ival(0);
		item=helper->GetItem(Storage::KEY_DATA);
		for(int j=0;j<item->rawval_size();j++)
		{
			PublishPtr data=PublishPtr(new MsgExpress::PublishData());
			if(data->ParseFromString(item->rawval(j)))
			{
				DataUnit* unit=GetUnit(setid,*data.get());
				if(unit)
				{
					vector<uint64> vecLost;
					unit->SyncData(data,vecLost);
				}
			}
			else
			{
				LOG4_ERROR("Parse failed,topic=%d",pub->topic());
			}
		}
	}
	else if(pub->topic()==Storage::TOPIC_DELDATA)
	{
		PublishHelperPtr helper=CreatePublishHelper(pub.get(),true);
		const MsgExpress::DataItem * item=helper->GetItem(Storage::KEY_DATA);
		for(int j=0;j<item->rawval_size();j++)
		{
			Storage::DeleteDataRequest request;
			if(request.ParseFromString(item->rawval(j)))
			{
				string unitkey=CreateKey(request.setid(),request.topic(),request.condition(),true);
				DataUnit* unit=NULL;
				sp_thread_mutex_lock(&mutex);
				if(mapData.find(unitkey)!=mapData.end())
					unit=mapData[unitkey];
				sp_thread_mutex_unlock(&mutex);
				if(unit)
				{
					unit->DeleteData(&request,false);
				}
			}
			else
			{
				LOG4_ERROR("Parse failed,topic=%d",pub->topic());
			}
		}
	}
}
