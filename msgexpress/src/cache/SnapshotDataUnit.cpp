#include "SnapshotDataUnit.h"
#include "DBMysql.h"


PublishPtr SnapshotDataUnit::Merge(PublishPtr old,PublishPtr data)
{
	mapTemp.clear();
	RepeatedPtrField<MsgExpress::DataItem>::const_iterator it=old->item().begin();
	while(it!=old->item().end())
	{
		mapTemp[it->key()]=&(*it);
		it++;
	}
	it=data->item().begin();
	while(it!=data->item().end())
	{
		mapTemp[it->key()]=&(*it);
		it++;
	}
	PublishPtr newOne=PublishPtr(new MsgExpress::PublishData(*old));
	uint64 id=old->id()+1;
	newOne->set_id(id);
	newOne->clear_item();
	std::map<int,const MsgExpress::DataItem*>::const_iterator it2=mapTemp.begin();
	while(it2!=mapTemp.end())
	{
		MsgExpress::DataItem* itm=newOne->add_item();
		itm->CopyFrom(*it2->second);
		it2++;
	}
	return newOne;
}
uint64 SnapshotDataUnit::PutData(PublishPtr data,bool loaddb,string& err)
{
	vector<string> keys;
	CreateKey(data->item(),false,keys);
	if(keys.size()<1)
	{
		err="key is empty";
		//LOG4_ERROR("key is empty:%s",data->ShortDebugString().c_str());
		return 0;
	}
	string key=keys[0];
	if(loaddb)
	{
		sp_thread_mutex_lock(&mutex);
		DataPtr item=DataPtr(new Data());
		item->data=data;
		item->status=Data::Synced;
		item->inDB=true;
		mapDataItem[key]=item;
		sp_thread_mutex_unlock(&mutex);
		return 0;
	}
	PublishPtr old;
	uint64 id=1;
	sp_thread_mutex_lock(&mutex);
	if(mapDataItem.find(key)!=mapDataItem.end())
	{
		DataPtr item=mapDataItem[key];
		item->status=Data::Syncing;
		item->data=Merge(mapDataItem[key]->data,data);
		id=item->data->id();
		if(item->inDB)
		    queueUnprocess.push(key);
		LOG4_INFO("Update data {dataset=%d,key=%s,id=%lld}",topic,key.c_str(),item->data->id());
	}
	else
	{
		id=1;
		data->set_id(id);
		DataPtr item=DataPtr(new Data());
		item->data=data;
		item->status=Data::Syncing;
		item->inDB=false;
		mapDataItem[key]=item;
		queueUnprocess.push(key);
		LOG4_INFO("New data {dataset=%d,key=%s,id=%lld}",topic,key.c_str(),item->data->id());
	}
	sp_thread_mutex_unlock(&mutex);
	data->set_id(id);
	return id;
}
void SnapshotDataUnit::SyncData(PublishPtr data,vector<uint64>& vecLost)
{
	vector<string> keys;
	CreateKey(data->item(),false,keys);
	if(keys.size()<1)
	{
		//LOG4_ERROR("key is empty:%s",data->ShortDebugString().c_str());
		return ;
	}
	string key=keys[0];
	PublishPtr old;
	sp_thread_mutex_lock(&mutex);
	if(mapDataItem.find(key)!=mapDataItem.end())
	{
		old=mapDataItem[key]->data;
		if(data->id()>old->id())
		{
			DataPtr item=mapDataItem[key];
			item->status=Data::Synced;
			item->inDB=true;
			item->data=Merge(old,data);
			LOG4_INFO("Sync data {dataset=%d,key=%s,id=%lld}",topic,key.c_str(),item->data->id());
		}
		else
			LOG4_WARN("{dataset=%d,key=%s,id=%lld} is old,ignore!",topic,key.c_str(),data->id());
	}
	else
	{
		DataPtr item=DataPtr(new Data());
		item->data=data;
		item->status=Data::Synced;
		item->inDB=true;
		mapDataItem[key]=item;
		LOG4_INFO("Sync data {dataset=%d,key=%s,id=%lld}",topic,key.c_str(),item->data->id());
	}
	sp_thread_mutex_unlock(&mutex);
}
uint32 SnapshotDataUnit::QueryData(Storage::QueryDataRequest* request,vector<PublishPtr>& dataset)
{
	vector<string> keys;
	CreateKey(request->condition(),true,keys);
	sp_thread_mutex_lock(&mutex);
	for(uint32 i=0;i<keys.size();i++)
	{
		if(mapDataItem.find(keys.at(i))!=mapDataItem.end())
		{
			PublishPtr msg=mapDataItem[keys.at(i)]->data;
			if(msg!=NULL)
				dataset.push_back(msg);
		}
	}
	sp_thread_mutex_unlock(&mutex);
	return 0;
}
uint32 SnapshotDataUnit::GetAllData(vector<PublishPtr>& dataset)
{
	sp_thread_mutex_lock(&mutex);
	dataset.reserve(mapDataItem.size());
	std::unordered_map<string,DataPtr>::const_iterator it=mapDataItem.begin();
	while(it!=mapDataItem.end())
	{
		dataset.push_back(it->second->data);
		it++;
	}
	sp_thread_mutex_unlock(&mutex);
	return 0;
}
int32 SnapshotDataUnit::DeleteData(Storage::DeleteDataRequest* request,bool deleteDB)
{
	vector<string> keys;
	CreateKey(request->condition(),true,keys);
	sp_thread_mutex_lock(&mutex);
	if(keys.size()<1)
	{
		LOG4_INFO( "Erase all data,dataset=%d,tablename=%s",topic,tablename.c_str() );
		mapDataItem.clear();
		while(queueUnprocess.size()>0)
		    queueUnprocess.pop();
#ifdef MYSQLSUPPORT
		//delete from database;
		if(deleteDB)
		{
			char buf[256];
			strprintf(buf,256,"delete from %s where `1`!=''",tablename.c_str());
			string sql(buf);
			bool ret=false;
			for(int i=0;i<2;i++)
			{
				ret=Database::DBMysql::GetInstance()->ExecuteSql(sql);
				if(!ret)
					sleep(1);
				else
					break;
			}
			if(!ret)
			{
				string err=Database::DBMysql::GetInstance()->GetErrorInfo();
				LOG4_ERROR( "Delete all from table[%s] failed,err:%s ",tablename.c_str(),err.c_str() );
			}
			else
			{
				LOG4_WARN( "Delete all from table[%s] success",tablename.c_str());
			}
		}
#endif
		LOG4_INFO( "Finished erase all data,dataset=%d,tablename=%s",topic,tablename.c_str() );
	}
	else
	{
		for(uint32 i=0;i<keys.size();i++)
		{
			string key=keys.at(i);
			std::unordered_map<string,DataPtr>::iterator it=mapDataItem.find(key);
			if(it!=mapDataItem.end())
		    {
				LOG4_INFO( "Delete data,dataset=%d,key=%s,tablename=%s,inDB=%d,deleteDB=%d",topic,keys.at(i).c_str(),tablename.c_str(),it->second->inDB,deleteDB );
				it->second->status=Data::Deleting;
				if(deleteDB)
				{
					if(it->second->inDB)
				        queueUnprocess.push(key);
				}
				else
					mapDataItem.erase(it);
			}
			else
				LOG4_WARN( "Delete data,but data not exists,dataset=%d,key=%s,tablename=%s",topic,keys.at(i).c_str(),tablename.c_str() );
		}
	}
	sp_thread_mutex_unlock(&mutex);
	return 0;
}
void SnapshotDataUnit::SaveDB()
{
#ifdef MYSQLSUPPORT
	int count=0;
	char buf[1024];
	string sql;
	string key;
	while(true)
	{
		if(queueUnprocess.size()<1)
			break;
		Data::Status status;
		bool inDB=false;
		PublishPtr data;
		string key;
		sp_thread_mutex_lock(&mutex);
		if(queueUnprocess.size()>0)
		{
		    key=queueUnprocess.front();
		    queueUnprocess.pop();
			std::unordered_map<string,DataPtr>::const_iterator it=mapDataItem.find(key);
			if(it==mapDataItem.end())
			{
				 LOG4_WARN( "key=%s not exits,why? dataset=%d,tablename=%s",key.c_str(),topic,tablename.c_str() );
			}
			else
			{
				DataPtr item=it->second;
				if(item==NULL || item->data==NULL)
				{
					LOG4_WARN("NULL item");
				}
				else
				{
					LOG4_INFO( "Begin process database,key=%s,status=%d,inDB=%d,dataset=%d,tablename=%s",key.c_str(),item->status,item->inDB,topic,tablename.c_str() );
					data=PublishPtr(new MsgExpress::PublishData(*item->data));
					status=item->status;
					inDB=item->inDB;
					item->status=Data::Synced;
					item->inDB=true;
					if(status==Data::Deleting)
						mapDataItem.erase(it);
				}
			}
		}
		sp_thread_mutex_unlock(&mutex);
		if(data==NULL)
			break;
		if(status==Data::Synced)
		{
			LOG4_WARN( "Data has been saved to database,dataset=%d,key=%s, tablename=%s",topic,key.c_str(),tablename.c_str() );
			continue;
		}
		int topic=data->topic();
		string uuid=data->uuid();
		uint64 id=data->id();
		string condition="";
		bool insert=false;
		bool update=false;
		bool delet=false;
		string printstr;
		if(status==Data::Syncing)
		{
			if(!inDB)
			{
				insert=true;
			    strprintf(buf,1024,"insert into %s set updatetime=now(), id=%lld",tablename.c_str(),id);
			}
		    else
			{
				update=true;
			    strprintf(buf,1024,"update %s set updatetime=now(),id=%lld",tablename.c_str(),id);
			}
		}
		else if(status==Data::Deleting)
		{
			if(inDB)
			{
				delet=true;
			    strprintf(buf,1024,"delete from %s ",tablename.c_str());
			}
			else
				break;
		}
		sql=buf;
		RepeatedPtrField<MsgExpress::DataItem>::const_iterator it=data->item().begin();
		while(it!=data->item().end())
		{
			if(it->ispk()==false)//
			{
				it++;
				continue;
			}
			string str;
			if(it->ival_size()>0 )
				strprintf(buf,1024,",`%d`=%d",it->key(),it->ival(0));
			else if(it->uival_size()>0)
				strprintf(buf,1024,",`%d`=%d",it->key(),it->uival(0));
			else if(it->lval_size()>0)
				strprintf(buf,1024,",`%d`=%lld",it->key(),it->lval(0));
			else if(it->ulval_size()>0)
				strprintf(buf,1024,",`%d`=%lld",it->key(),it->ulval(0));
			else if(it->fval_size()>0)
				strprintf(buf,1024,",`%d`=%f",it->key(),it->fval(0));
			else if(it->dval_size()>0)
				strprintf(buf,1024,",`%d`=%f",it->key(),it->dval(0));
			else if(it->bval_size()>0)
				strprintf(buf,1024,",`%d`=%d",it->key(),it->bval(0)?1:0);
			else if(it->tval_size()>0)
			{
				time_t t=it->tval(0);
				struct tm *local=localtime(&t);
				char buffer[128];
				strftime(buffer,128,"%Y-%m-%d %H:%M:%S",local);
				strprintf(buf,1024,",`%d`='%s'",it->key(),buffer);
			}
			else if(it->strval_size()>0)
			{
				strprintf(buf,1024,",`%d`='",it->key());
				str.append(it->strval(0));
				str.append("'");
			}
			else if(it->rawval_size()>0)
			{
				strprintf(buf,1024,",`%d`='",it->key());
				string data=Database::DBMysql::GetInstance()->GetConvertBytesString(it->rawval(0));
				str.append(data);
				str.append("'");
			}
			else
			{
				it++;
				continue;
			}
			if(insert || (update && it->ispk()==false))
			{
				sql.append(buf);
				if(str.size()>0)
					sql.append(str);
			}
			if((update || delet) && it->ispk())
			{
				if(condition.size()==0)
				    condition.append(&buf[1]);
				else{
					condition.append(" and ");
					condition.append(&buf[1]);
				}
				if(str.size()>0)
				    condition.append(str);
			}
			it++;
		}
		printstr=sql;
		if(insert || update)
		{
			sql.append(",content='");
			string content=Database::DBMysql::GetInstance()->GetConvertBytesString(data->SerializeAsString());
			sql.append(content);
			sql.append("'");
		}
		if( update || delet)
		{
			sql.append(" where "+condition);
			printstr.append(" where "+condition);
		}
	    bool ret=Database::DBMysql::GetInstance()->ExecuteSql(sql);
		if(ret)
		{
			LOG4_INFO( "process database success,sql=%s ",printstr.c_str() );
		}
		else
		{
			string err=Database::DBMysql::GetInstance()->GetErrorInfo();
			LOG4_ERROR( "process database failed,err:%s,data: ",err.c_str() );
			break;
		}
		count++;
	}
#endif
}

void SnapshotDataUnit::Print()
{
	sp_thread_mutex_lock(&mutex);
	if(mapDataItem.size()>0){
		LOG4_INFO( "Table:%s,Total item=%d ",tablename.c_str(),mapDataItem.size() );
		std::unordered_map<string,DataPtr>::const_iterator it=mapDataItem.begin();
		while(it!=mapDataItem.end())
		{
			LOG4_DEBUG( "key=%s,id=%d ",it->first.c_str(),it->second->data->id());
			it++;
		}
	}
	sp_thread_mutex_unlock(&mutex);
}

SnapshotDataUnit::SnapshotDataUnit(string key,int topic,string tbl)
{
	unitkey=key;
	this->topic=topic;
	//char tmp[128];
	//strprintf(tmp,"snapshot%d",topic);
	tablename=tbl;
	sp_thread_mutex_init(&mutex,NULL);
	mapDataItem.rehash(1000000);
}


SnapshotDataUnit::~SnapshotDataUnit(void)
{
	sp_thread_mutex_destroy(&mutex);
}

void SnapshotDataUnit::CreateKey(const RepeatedPtrField<MsgExpress::DataItem>& datalist,bool isQuery,vector<string>& vecKeys)
{
	if(isQuery)
	{
		vector<string> temp;
		string key="k";
		vecKeys.push_back(key);
		char buf[32];

		RepeatedPtrField<MsgExpress::DataItem>::const_iterator it=datalist.begin();
		while(it!=datalist.end())
		{
			if(it->ival_size()>0 )
			{
				for(uint32 i=0;i<vecKeys.size();i++)
				{
					for(int j=0;j<it->ival_size();j++)
					{
						strprintf(buf,32,"-%d",it->ival(j));
						temp.push_back(vecKeys.at(i)+buf);
					}
				}
			}
			else if(it->uival_size()>0)
			{
				for(uint32 i=0;i<vecKeys.size();i++)
				{
					for(int j=0;j<it->uival_size();j++)
					{
						strprintf(buf,32,"-%d",it->uival(j));
						temp.push_back(vecKeys.at(i)+buf);
					}
				}
			}
			else if(it->lval_size()>0)
			{
				for(uint32 i=0;i<vecKeys.size();i++)
				{
					for(int j=0;j<it->lval_size();j++)
					{
						strprintf(buf,32,"-%lld",it->lval(j));
						temp.push_back(vecKeys.at(i)+buf);
					}
				}
			}
			else if(it->ulval_size()>0)
			{
				for(uint32 i=0;i<vecKeys.size();i++)
				{
					for(int j=0;j<it->ulval_size();j++)
					{
						strprintf(buf,32,"-%lld",it->ulval(j));
						temp.push_back(vecKeys.at(i)+buf);
					}
				}
			}
			else if(it->bval_size()>0)
			{
				for(uint32 i=0;i<vecKeys.size();i++)
				{
					for(int j=0;j<it->bval_size();j++)
					{
						strprintf(buf,32,"-%d",it->bval(j)?1:0);
						temp.push_back(vecKeys.at(i)+buf);
					}
				}
			}
			else if(it->strval_size()>0)
			{
				for(uint32 i=0;i<vecKeys.size();i++)
				{
					for(int j=0;j<it->strval_size();j++)
					{
						strprintf(buf,32,"-%s",it->strval(j).c_str());
						temp.push_back(vecKeys.at(i)+buf);
					}
				}
			}
			vecKeys.clear();
			vecKeys=temp;
			temp.clear();
			it++;
		}
	}
	else
	{
		string key="k";
		char buf[32];
		RepeatedPtrField<MsgExpress::DataItem>::const_iterator it=datalist.begin();
		while(it!=datalist.end())
		{
			if(it->ispk())
			{
				if(it->ival_size()>0 )
					strprintf(buf,32,"-%d",it->ival(0));
				else if(it->uival_size()>0)
					strprintf(buf,32,"-%d",it->uival(0));
				else if(it->lval_size()>0)
					strprintf(buf,32,"-%lld",it->lval(0));
				else if(it->ulval_size()>0)
					strprintf(buf,32,"-%lld",it->ulval(0));
				else if(it->bval_size()>0)
					strprintf(buf,32,"-%d",it->bval(0)?1:0);
				else if(it->strval_size()>0)
					strprintf(buf,32,"-%s",it->strval(0).c_str());
				key.append(buf);
			}
			it++;
		}
		vecKeys.push_back(key);
	}
	if(vecKeys.size()==1 && vecKeys.at(0)=="k")
		vecKeys.clear();
}