#ifndef _DATAMANAGER_H_
#define _DATAMANAGER_H_


#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <list>
#include <unordered_map>
#include <queue>
#include "../core/spthread.h"
#include "SeriesDataUnit.h"
#include "SnapshotDataUnit.h"
#include "../protobuf/protobuf.h"
#include "DataUnit.h"
#include "IDataManager.h"

class MSGEXPRESS_API DataManager : public IDataManager
{
	struct SetInfo
	{
		enum:unsigned char{
			TimeSeries=1,
			Snapshot=2,
		};
		int setid;
		unsigned char type;
		string tablename;
		vector<int> keylist;
		vector<int> collist;
	};
private:
	std::unordered_map<int,SetInfo*> mapSetInfo;

	sp_thread_mutex_t mutex;
	std::unordered_map<string,DataUnit*> mapData;//<topic-keys,maxid>

	sp_thread_semaphore_t semph;
	std::unordered_map<string,DataUnit*> mapUnSaved;
	bool run;
public:
	DataManager();
	virtual ~DataManager();
private:
	void onRequest(MessagePtr request,Message*& response);
	string CreateKey(int setid,int topic,const RepeatedPtrField<MsgExpress::DataItem>& datalist,bool isQuery);

	static sp_thread_result_t SP_THREAD_CALL  saveDBThread(void* arg);
	static sp_thread_result_t SP_THREAD_CALL  printThread(void* arg);
	void processDB();
	void print();
	
	DataUnit* GetUnit(int setid,const MsgExpress::PublishData& savedata);
public:
	virtual void LoadDB();
    virtual void GetSetInfo(Storage::SetInfoRequest* req,Storage::SetInfoResponse* resp);
	virtual void SetSetInfo(Storage::SetInfoResponse* resp);
	virtual int PutData(Storage::SaveDataRequest* req,string& err,uint64& id);
	virtual int DelData(Storage::DeleteDataRequest* req,string& err);
	virtual int QueryData(Storage::QueryDataRequest* req,string& err,vector<PublishPtr>& dataset);
	virtual void SyncData(PublishPtr data);
};
#endif
