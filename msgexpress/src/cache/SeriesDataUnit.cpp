#include "SeriesDataUnit.h"
#include "DBMysql.h"

SeriesDataUnit::SeriesDataUnit(string key,int t,string tbl)
{
	unitkey=key;
	topic=t;
	/*char tmp[128];
	strprintf(tmp,128,"tbl%d",topic);*/
	tablename=tbl;
	sp_thread_mutex_init(&mutex,NULL);
	mapDataItem.rehash(1000000);
	mapIdItem.rehash(1000000);
	maxId=0;
	LoadData();
}

SeriesDataUnit::~SeriesDataUnit(void)
{
	sp_thread_mutex_destroy(&mutex);
}

void SeriesDataUnit::Print()
{
}

void SeriesDataUnit::LoadData()
{
#ifdef MYSQLSUPPORT
	//char tmp[128];
	//strprintf(tmp,128,"select maxid from idinfo where unitkey='%s'",unitkey.c_str());
	//string sql=tmp;
	//Database::CRecordSet result;
	//bool ret=Database::DBMysql::GetTransactionInstance()->QueryAndGetResult(sql,result);
	//if(ret && result.GetRowNum()>0)
	//{
	//	string data=result.GetDataAt(0,0);
	//	maxId=atoi(data.c_str());
	//}
	//else
	//{
	//	//LOG4_ERROR("Load data from idInfo failed");
	//	return;
	//}
#endif
}

uint64 SeriesDataUnit::PutData(PublishPtr data,bool loaddb,string& err)
{
	string uuid=data->uuid();
	if(uuid.length()<8)
	{
		err="Request uuid is too short";
		//LOG4_ERROR("Request uuid is  too short:%s",data->ShortDebugString().c_str());
		return 0;
	}
	if(loaddb)
	{
		sp_thread_mutex_lock(&mutex);
		mapDataItem[uuid]=data;
		mapIdItem[data->id()]=data;
		if(data->id()>maxId)
			  maxId=data->id();
		sp_thread_mutex_unlock(&mutex);
		return 0;
	}
	uint64 id=0;
	sp_thread_mutex_lock(&mutex);
	if(mapDataItem.find(uuid)!=mapDataItem.end())
	{
		LOG4_WARN("Duplicated uuid:%s",uuid.c_str());
		id=mapDataItem[uuid]->id();
	}
	else
	{
		maxId++;
		id=maxId;
		PublishPtr pubData=PublishPtr(new MsgExpress::PublishData(*data));
		pubData->set_id(id);
		mapDataItem[uuid]=pubData;
		mapIdItem[id]=pubData;
		queueUnsaved.push(uuid);
	}
	sp_thread_mutex_unlock(&mutex);
	data->set_id(id);
	return id;
}
void SeriesDataUnit::SyncData(PublishPtr data,vector<uint64>& vecLost)
{
	string uuid=data->uuid();
	if(uuid.length()<8)
	{
		//LOG4_ERROR("Request uuid is  too short:%s",data->ShortDebugString().c_str());
		return ;
	}
	sp_thread_mutex_lock(&mutex);
	if(mapDataItem.find(uuid)!=mapDataItem.end())
	{
		LOG4_WARN("Duplicated uuid:%s",uuid.c_str());
	}
	else
	{
		//LOG4_WARN("uuid:%s,id=%lld",data->uuid().c_str(),data->id());
		if(data->id()>maxId)
		{
			for(uint64 i=maxId+1;i<data->id();i++)
			{
				vecLost.push_back(i);
				LOG4_WARN("Lost data,uuid:%s,id=%lld",data->uuid().c_str(),i);
			}
			maxId=data->id();
		}
		PublishPtr pubData=PublishPtr(new MsgExpress::PublishData(*data));
		mapDataItem[uuid]=pubData;
		mapIdItem[data->id()]=pubData;
	}
	sp_thread_mutex_unlock(&mutex);
}
uint32 SeriesDataUnit::QueryData(Storage::QueryDataRequest* request,vector<PublishPtr>& dataset)
{
	uint64 from=request->from();
	uint64 to=request->to();
	uint32 num=request->num();

	if(from>0 && to==0)
		to=from+num-1;
	else if(from==0 && to>0)
		from=((int64)to-(int64)num+1)>0?(to-num+1):1;
	else if(from==0 && to==0)
	{
		from=((int64)maxId-num+1)>0?((int64)maxId-num+1):1;
		to=maxId;
	}
	if(to>maxId)
		to=maxId;
	sp_thread_mutex_lock(&mutex);
	for(uint64 i=to;i>=from;i--)
	{
		if(mapIdItem.find(i)!=mapIdItem.end())
			dataset.push_back(mapIdItem[i]);
	}
	sp_thread_mutex_unlock(&mutex);
	return dataset.size();
}
uint32 SeriesDataUnit::GetAllData(vector<PublishPtr>& dataset)
{
	return 0;
}
int32 SeriesDataUnit::DeleteData(Storage::DeleteDataRequest*,bool deleteDB)
{
	return 0;
}
void SeriesDataUnit::SaveDB()
{
#ifdef MYSQLSUPPORT
	int count=0;
	char buf[1024];
	string sql;
	uint64 maxid=0;
	while(true)
	{
		string uuid;
		uint64 id=0;
		PublishPtr data;
		bool saved=false;
		sp_thread_mutex_lock(&mutex);
		if(queueUnsaved.size()>0)
		{
			uuid=queueUnsaved.front();
			data=mapDataItem[uuid];
			id=data->id();
			//下面是检查一批数据的id是否单调递增的，理论上应该是这样的
			if(id>maxid)
				maxid=id;
			else
				LOG4_ERROR("(maxid=%lld)>=(cur id=%lld),why",maxid,id);
		}
		sp_thread_mutex_unlock(&mutex);
		if(data==NULL)
			break;
		//LOG4_ERROR("SaveData,unitkey=%s,uuid=%s,id=%lld",unitkey.c_str(),uuid.c_str(),id);
		if(!saved)
		{
			int topic=data->topic();
			strprintf(buf,1024,"insert into %s set uuid='%s',id=%lld,`key`='%s'",tablename.c_str(),uuid.c_str(),id,unitkey.c_str());
			sql=buf;
			/*RepeatedPtrField<MsgExpress::DataItem>::const_iterator it=data->item().begin();
			while(it!=data->item().end())
			{
				string str;
				if(it->ival_size()>0 )
					strprintf(buf,1024,",`%d`=%d",it->key(),it->ival(0));
				else if(it->uival_size()>0)
					strprintf(buf,1024,",`%d`=%d",it->key(),it->uival(0));
				else if(it->lval_size()>0)
					strprintf(buf,1024,",`%d`=%lld",it->key(),it->lval(0));
				else if(it->ulval_size()>0)
					strprintf(buf,1024,",`%d`=%lld",it->key(),it->ulval(0));
				else if(it->fval_size()>0)
					strprintf(buf,1024,",`%d`=%f",it->key(),it->fval(0));
				else if(it->dval_size()>0)
					strprintf(buf,1024,",`%d`=%f",it->key(),it->dval(0));
				else if(it->bval_size()>0)
					strprintf(buf,1024,",`%d`=%d",it->key(),it->bval(0)?1:0);
				else if(it->tval_size()>0)
				{
					time_t t=it->tval(0);
					struct tm *local=localtime(&t);
					char buffer[128];
					strftime(buffer,128,"%Y-%m-%d %H:%M:%S",local);
					strprintf(buf,1024,",`%d`='%s'",it->key(),buffer);
				}
				else if(it->strval_size()>0)
				{
					strprintf(buf,1024,",`%d`='",it->key());
					str.append(it->strval(0));
					str.append("'");
				}
				else if(it->rawval_size()>0)
				{
					strprintf(buf,1024,",`%d`='",it->key());
					string data=Database::DBMysql::GetInstance()->GetConvertBytesString(it->rawval(0));
					str.append(data);
					str.append("'");
				}
				else
				{
					it++;
					continue;
				}
				it++;
				sql.append(buf);
				if(str.size()>0)
					sql.append(str);
			}*/
			sql.append(",content='");
			string content=Database::DBMysql::GetInstance()->GetConvertBytesString(data->SerializeAsString());
			sql.append(content);
			sql.append("'");
	        bool ret=Database::DBMysql::GetInstance()->ExecuteSql(sql);
			if(ret)
			{

			}
			else
			{
				string err=Database::DBMysql::GetInstance()->GetErrorInfo();
				LOG4_ERROR( "save data failed,err:%s,data: ",err.c_str() );
				break;
			}
			
			if(id==1)
			{
				strprintf(buf,1024,"insert into idinfo(maxid,unitkey) values(%lld,'%s')",id ,unitkey.c_str());
				sql=buf;
				ret=Database::DBMysql::GetInstance()->ExecuteSql(sql);
			}

			sp_thread_mutex_lock(&mutex);
			queueUnsaved.pop();
			sp_thread_mutex_unlock(&mutex);
		}
		count++;
	}
	if(maxid>1)
	{
		strprintf(buf,1024,"update idinfo set maxid=%lld where unitkey='%s'",maxid,unitkey.c_str());
		sql=buf;
		bool ret=Database::DBMysql::GetInstance()->ExecuteSql(sql);
	}
#endif
}
