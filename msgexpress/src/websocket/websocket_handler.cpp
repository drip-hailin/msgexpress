#include "../core/porting.h"
#include "websocket_handler.h"

Websocket_Handler::Websocket_Handler(int fd):
		//buff_(),
		status_(WEBSOCKET_UNCONNECT),
		header_map_(),
		fd_(fd)
{
}

Websocket_Handler::~Websocket_Handler(){
}


int Websocket_Handler::process(struct evbuffer *evbuf, string& retStr){
	size_t total = evbuffer_get_length(evbuf);
	string inFrame;
	inFrame.resize(total);
	evbuffer_copyout(evbuf, (char*)inFrame.data(), total);
	
	if (status_ == WEBSOCKET_UNCONNECT){
		evbuffer_drain(evbuf, total);
		return handshark(inFrame, retStr);
	}
	int ret = decodeFrame(inFrame, retStr);
	if (WS_OPENING_FRAME == ret)
	{
		evbuffer_drain(evbuf, total);
	}
	else if (0 == ret)
	{
		return -1;
	}
	else
	{
		evbuffer_drain(evbuf, total);
		LOG4_ERROR("Websocket decode failed.");
		return -1;
	}
	return 0;
}


int Websocket_Handler::handshark(const string& data, string& outStr){
	char request[1024] = {};
	status_ = WEBSOCKET_HANDSHARKED;
	int ret=fetch_http_info(data);
	if (ret != 0)
		return ret;
	parse_str(request);
	outStr = request;
	return send_data(outStr);
}

int Websocket_Handler::decodeFrame(const string& inFrame, string &outMessage)
{
	uint8_t type = static_cast<uint8_t >(inFrame[0] & 0x0f);
	LOGGER_INFO("decode websocket frame,size=" << inFrame.size() );
	int ret = WS_OPENING_FRAME;
	const char *frameData = inFrame.c_str();
	const int frameLength = inFrame.size();
	if (frameLength < 2)
	{
		ret = WS_ERROR_FRAME;
	}

	// 检查扩展位并忽略  
	if ((frameData[0] & 0x70) != 0x0)
	{
		ret = WS_ERROR_FRAME;
		LOGGER_ERROR("receive error from websocket,1");
	}

	// fin位: 为1表示已接收完整报文, 为0表示继续监听后续报文  
	if ((frameData[0] & 0x80) != 0x80)
	{
		ret = WS_ERROR_FRAME;
		LOGGER_ERROR("receive error from websocket,2");
	}

	// mask位, 为1表示数据被加密  
	if ((frameData[1] & 0x80) != 0x80)
	{
		ret = WS_ERROR_FRAME;
		LOGGER_ERROR("receive error from websocket,3");
	}

	// 操作码  
	uint32_t payloadLength = 0;
	uint8_t payloadFieldExtraBytes = 0;
	uint8_t opcode = static_cast<uint8_t >(frameData[0] & 0x0f);
	if (opcode == WS_BINARY_FRAME || opcode == WS_TEXT_FRAME)
	{
		// 处理utf-8编码的文本帧  
		payloadLength = static_cast<uint16_t >(frameData[1] & 0x7f);
		if (payloadLength == 0x7e)
		{
			uint16_t payloadLength16b = 0;
			payloadFieldExtraBytes = 2;
			memcpy(&payloadLength16b, &frameData[2], payloadFieldExtraBytes);
			payloadLength = ntohs(payloadLength16b);
		}
		else if (payloadLength == 0x7f)
		{
			payloadFieldExtraBytes = 8;
			payloadLength = (frameData[6] << 24) + (frameData[7] << 16) + (frameData[8] << 8) + frameData[9];
		}
		if (inFrame.size() < payloadLength + payloadFieldExtraBytes + 6)
		{
			LOGGER_INFO("need continue receive from websocket...,payload=" << payloadLength << ",frame size=" << frameLength);
			return 0;
		}
		LOGGER_INFO("finish receive a  msg from websocket,payload=" << payloadLength << ",frame size=" << frameLength);
	}
	else if (opcode == WS_PING_FRAME || opcode == WS_PONG_FRAME)
	{
		// /ping/pong帧暂不处理  
		LOGGER_ERROR("receive ping/pong frame from websocket");
	}
	else if (opcode == WS_CLOSING_FRAME)
	{
		ret = WS_CLOSING_FRAME;
		LOGGER_ERROR("receive close frame from websocket");
	}
	else
	{
		ret = WS_ERROR_FRAME;
		LOGGER_ERROR("receive error from websocket,4");
	}

	// 数据解码  
	if ((ret != WS_ERROR_FRAME) && (payloadLength > 0))
	{
		// header: 2字节, masking key: 4字节  
		const char *maskingKey = &frameData[2 + payloadFieldExtraBytes];
		outMessage.resize(payloadLength ,0);
		memcpy((char*)outMessage.data(), &frameData[2 + payloadFieldExtraBytes + 4], payloadLength);
		for (int i = 0; i < payloadLength; i++)
		{
			outMessage[i] = outMessage[i] ^ maskingKey[i % 4];
		}
	}

	return ret;
}


int Websocket_Handler::encodeFrame(const string& inMessage, string &outFrame, enum WS_FrameType frameType)
{
	// 构造websocket头
	uint8_t wsMakeFrameHead[20] = { 0 };
	size_t FrameHeadLen = 0;
	const size_t  messageLength = inMessage.size();

	wsMakeFrameHead[0] = 0x80 | frameType;

	if (messageLength < 0x7e) {
		wsMakeFrameHead[1] = messageLength;
		FrameHeadLen = 2;
	}
	else if (messageLength <= 0xFFFF) {
		wsMakeFrameHead[1] = 0x7e;
		uint16_t payloadLength16b = htons((u_short)messageLength);
		memcpy(&wsMakeFrameHead[2], &payloadLength16b, 2);
		FrameHeadLen = 4;
	}
	else {
		wsMakeFrameHead[1] = 0x7f;
		wsMakeFrameHead[2] = 0;
		wsMakeFrameHead[3] = 0;
		wsMakeFrameHead[4] = 0;
		wsMakeFrameHead[5] = 0;
		wsMakeFrameHead[6] = (uint8_t)(messageLength >> 24);
		wsMakeFrameHead[7] = (uint8_t)(messageLength >> 16);
		wsMakeFrameHead[8] = (uint8_t)(messageLength >> 8);
		wsMakeFrameHead[9] = (uint8_t)(messageLength & 0xFF);
		FrameHeadLen = 10;
		LOGGER_INFO("send a big msg to websocket" << messageLength);
	}
	// 填充数据  
	uint32_t frameSize = FrameHeadLen + messageLength;
	outFrame.resize(frameSize);
	memcpy((char*)outFrame.data(), wsMakeFrameHead, FrameHeadLen);
	memcpy((char*)outFrame.data() + FrameHeadLen, inMessage.data(), messageLength);
	LOGGER_INFO("send a msg to websocket,length=" << frameSize);
	return 0;
}

void Websocket_Handler::parse_str(char *request){  
	strcat(request, "HTTP/1.1 101 Switching Protocols\r\n");
	strcat(request, "Connection: upgrade\r\n");
	strcat(request, "Sec-WebSocket-Accept: ");
	std::string server_key = header_map_["Sec-WebSocket-Key"];
	server_key += MAGIC_KEY;

	SHA1 sha;
	unsigned int message_digest[5];
	sha.Reset();
	sha << server_key.c_str();

	sha.Result(message_digest);
	for (int i = 0; i < 5; i++) {
		message_digest[i] = htonl(message_digest[i]);
	}
	server_key = CodeBase64::Encode(reinterpret_cast<const unsigned char*>(message_digest), 20);
	server_key += "\r\n";
	strcat(request, server_key.c_str());
	strcat(request, "Upgrade: websocket\r\n\r\n");
}

int Websocket_Handler::fetch_http_info(const string& data){

	std::istringstream s(data);
	std::string request;

	std::getline(s, request);
	if (request[request.size()-1] == '\r') {
		request.erase(request.end()-1);
	} else {
		return -1;
	}

	std::string header;
	std::string::size_type end;

	while (std::getline(s, header) && header != "\r") {
		if (header[header.size()-1] != '\r') {
			continue; //end
		} else {
			header.erase(header.end()-1);	//remove last char
		}

		end = header.find(": ",0);
		if (end != std::string::npos) {
			std::string key = header.substr(0,end);
			std::string value = header.substr(end+2);
			header_map_[key] = value;
		}
	}

	return 0;
}

int Websocket_Handler::encodeMsg(const string& msg, string& outStr)
{
	return encodeFrame(msg, outStr, WS_BINARY_FRAME);
}

int Websocket_Handler::sendMsg(const string& msg){
	string outMsg;
	encodeFrame(msg, outMsg, WS_TEXT_FRAME);
	return send_data(outMsg);
}

int Websocket_Handler::send_data(const string& msg){
	iovec vec;
	vec.iov_base = (char*)msg.data();
	vec.iov_len = msg.size();
	int count= sp_writev(fd_, &vec, 1);
	if (count > 0)
		return 0;
	return -1;
}
