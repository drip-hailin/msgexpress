#pragma once

#include <hash_map>
#include "../inc/msgexpress.h"
#include "../proxy/Proxy.h"


class MSGEXPRESS_API DesktopGatewayProxy : public MsgExpressProxy
{
private:
	sp_thread_mutex_t mMutex;
	string cfgfile;
private:
	void ProcessMyMessage(PackagePtr package);
	void ProcessPublish(PackagePtr package);
public:
	bool StartA(const char* configFile,ProxyCFG* proxyConfig);
	bool StartB(const char* configFile);
	DesktopGatewayProxy();
	virtual ~DesktopGatewayProxy();
protected:
	virtual void OnRawPackageA(PackagePtr package);
	virtual void OnEventA(int eventId);

	virtual void OnRawPackageB(PackagePtr package);
	virtual void OnEventB(int eventId);
};

