#ifndef _SERVICEMANAGER_H_
#define _SERVICEMANAGER_H_

#ifdef _WIN32 
#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#define _CRTDBG_MAP_ALLOC
#endif
#include <assert.h>

#include <map>
#include <hash_map>
#include <set>
#include <vector>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "../core/utils.h"
#include "../core/spthread.h"
#include "../inc/package.h"
#include "broker_config.h"

using namespace std;

struct CountInfo
{
	uint64_t inRequest;
	uint64_t outRequest;
	uint64_t inResponse;
	uint64_t outResponse;
	uint64_t inPublish;
	uint64_t outPublish;
	uint64_t inRequestB;
	uint64_t outRequestB;
	uint64_t inResponseB;
	uint64_t outResponseB;
	uint64_t inPublishB;
	uint64_t outPublishB;
	uint64_t queueLength;
	CountInfo()
	{
		inRequest=outRequest=inResponse=outResponse=inPublish=outPublish=inRequestB=outRequestB=inResponseB=outResponseB=inPublishB=outPublishB=queueLength=0;
	}
};

class MSGEXPRESS_API ServiceManager
{
private:
	string cfgFilePath;
	ServerConfig serverCfg;

	unordered_map<AppAddress, CountInfo*, AppAddressHash> mMapCountInfo;//key是地址
	std::mutex mMutexCount;

	unordered_map<AppAddress, MsgExpress::AppInfo*, AppAddressHash> mMapAppInfo;//key是地址
	unordered_map<int, vector<AppAddress>*> mMapService;//服务提供者名单，key是服务号
	unordered_map<int, vector<AppAddress>*> mMapServiceProxy;//服务提供者代理名单，key是服务号
	unordered_map<int, AppAddress> mMapLoadbalance;//key是服务号
	unordered_map<int,bool> mMapMasterSlaveService;//key是服务号
	unordered_map<uint64_t,AppAddress> mMapServiceRent;//key是【address:service】,其中地址是使用方，value是服务提供方address
	unordered_map<AppAddress, vector<AppAddress>*, AppAddressHash> mMapServiceRenterList;//服务使用者名单，key是服务提供方地址，list是使用者名单
	std::mutex mMutexService;

#define MapSubscription unordered_map<AppAddress,vector<int>*,AppAddressHash>//key 是用户id
	MapSubscription mMapUserSubIdList;//key是用户id，
	unordered_map<string,MessagePtr> mMapUserSubInfo;//key是由用户id和subId构成
	unordered_map<string,MapSubscription*> mMapSubDetails;//订阅的详细信息,key是由订阅条件生成的字符串
	unordered_map<int,set<int> > mMapTopicKeys; 
	unordered_map<int, unordered_set<AppAddress, AppAddressHash>* > mMapTopicUsers;
	std::mutex mMutexSub;

private:
	static sp_thread_result_t SP_THREAD_CALL  fileMonitor(void* arg);
	void checkFile();
	void loadMasterSlaveService();
	void LogCallback(string level,string log);
public:
	ServiceManager();
	ServiceManager(string cfgfile);
	~ServiceManager();
	
	string getAuthData();
	int getRecvQueueSize();

	void regService(vector<int>& vecService,AppAddress sid);
	void unregService(unordered_set<int>& setService, AppAddress sid);
	AppAddress getServiceProvider(int serviceId, AppAddress srcAddr, MsgExpress::LoginInfo& info, unsigned short code);
	void getServiceProviderList(int serviceId, std::vector<AppAddress>& list, std::vector<AppAddress>& proxylist);

	void regApp(AppAddress addr, const MsgExpress::LoginInfo& appInfo);
	void unregApp(AppAddress addr);
	bool getAppInfo(AppAddress addr, MsgExpress::AppInfo& info);
	void getAppType(std::vector<AppAddress>& vecAddr, std::vector<int>& vecType);
	void getAppInfo(const MsgExpress::GetAppList& request,MsgExpress::AppList& result);
	void getAppList(MsgExpress::AppList& result);
	bool getCountInfo(AppAddress addr, CountInfo& countInfo);
	void updateCountInfo(AppAddress addr, const CountInfo& countInfo);
	void updateAppStatus(const MsgExpress::UpdateAppStatus& info);

	void subscribe(int topic, int subid, AppAddress addr, const vector<string>& vecCondition, const vector<int>& keys, MessagePtr subMsg);
	void subscribe(const MsgExpress::SubscribeData& sub, AppAddress addr);
	void subscribe(const MsgExpress::SimpleSubscription& sub, AppAddress addr);
	void unSubscribe(int subId, AppAddress sid);
	void unSubscribeAll(AppAddress sid);
	bool IsMyFood(MsgExpress::PublishData* pubData, AppAddress addr);
	void getSubscriberList(MsgExpress::PublishData* pubData, unordered_map<AppAddress, vector<MsgExpress::SubscribeData*>*, AppAddressHash>& mapUserSubList);
	unordered_set<AppAddress, AppAddressHash>* getSubscriberIdList(MsgExpress::PublishData* pubData);
	unordered_set<AppAddress, AppAddressHash>* getSubscriberIdList(int topic, const string& pubMsg);
	//unordered_set<AppAddress, AppAddressHash>* getSubscriberIdList(int topic);
	AppAddress getAddress(string uuid);
	string getBrokerName(){ return serverCfg.name; }
	//UserList * getUserList(){ return &mOnlineSidList; }
	string getServicesInfo();
	bool isMasterSlaveMode(unsigned int serviceId);
	std::vector<int> getMasterSlaveService();
};

#endif
