#pragma once

#include <string>
#include <mutex>
#include "../tinyxml/tinyxml.h"
#include "../inc/package.h"

using namespace std;

class MSGEXPRESS_API XMLConfig
{
private:
	static string mPath;
	bool loaded;
	std::mutex mtx;
public:
	XMLConfig();
public:
	~XMLConfig();
	static void SetPath(const string& path);
	bool LoadFile(string file);
	virtual bool ReadConfigFile() = 0;
public:
	TiXmlDocument *mDocument;
};
