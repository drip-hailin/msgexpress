#ifndef _MSGDECODER_H_
#define _MSGDECODER_H_

#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <assert.h>
#endif
#include "../inc/package.h"
#include "../core/utils.h"
#include <event2/bufferevent.h>
#include <event2/buffer.h>
class MSGEXPRESS_API MsgDecoder {
public:
	MsgDecoder(int queueSize=10,bool isServer=false);
	~MsgDecoder();

	virtual int decode2(struct evbuffer * inBuffer);
	Package* decode(struct evbuffer *);

	SafeQueue * getSequenceQueue();
	SafeQueue * getNormalQueue();
	SafeQueue * getPriorityQueue();
	//SafeQueue * getHeartbeatQueue();
	bool hasHeartbeat();
	int getQueueSize(){ return mSequenceQueue->getLength() + mNormalQueue->getLength(); }
private:
	SafeQueue * mNormalQueue;
	SafeQueue * mSequenceQueue;
	SafeQueue * mPriorityQueue;
	//SafeQueue * mHeartbeatQueue;
    bool heartbeat;
	bool isServer;
#ifdef DEBUG_CODE
	int totalCount;
#endif
};

#endif