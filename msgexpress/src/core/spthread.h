#pragma once

#ifndef _WIN32

#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <errno.h>

typedef void * sp_thread_result_t;

typedef pthread_t       sp_thread_t;
typedef pthread_attr_t  sp_thread_attr_t;
typedef sem_t           sp_thread_semaphore_t;
typedef unsigned long       DWORD;
const long BILLION = 1000000000;

//#define sp_thread_semaphore_init        sem_init
int sp_thread_semaphore_init(sp_thread_semaphore_t * semaphore,unsigned int initvalue,unsigned int maxvalue);
#define sp_thread_semaphore_destroy     sem_destroy
#define sp_thread_semaphore_post        sem_post
#define sp_thread_semaphore_wait        sem_wait
int sp_thread_semaphore_timedwait( sp_thread_semaphore_t * semaphore,DWORD dwMilliseconds);



#define sp_thread_attr_init           pthread_attr_init
#define sp_thread_attr_destroy        pthread_attr_destroy
#define sp_thread_attr_setdetachstate pthread_attr_setdetachstate
#define SP_THREAD_CREATE_DETACHED     PTHREAD_CREATE_DETACHED
#define sp_thread_attr_setstacksize   pthread_attr_setstacksize

#define sp_thread_self    pthread_self
#define sp_thread_create  pthread_create

#define SP_THREAD_CALL
typedef sp_thread_result_t ( * sp_thread_func_t )( void * args );


#else ///////////////////////////////////////////////////////////////////////

// win32 thread

#include <winsock2.h>
#include <process.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long long sp_thread_t;

typedef unsigned sp_thread_result_t;
#define SP_THREAD_CALL __stdcall
typedef sp_thread_result_t ( __stdcall * sp_thread_func_t )( void * args );

typedef HANDLE  sp_thread_semaphore_t;

typedef struct tagsp_thread_attr {
	int stacksize;
	int detachstate;
} sp_thread_attr_t;

#define SP_THREAD_CREATE_DETACHED 1


int sp_thread_semaphore_init(sp_thread_semaphore_t * semaphore,unsigned int initvalue,unsigned int maxvalue);
int sp_thread_semaphore_destroy(sp_thread_semaphore_t * semaphore);
//释放信号量，此时信号量的数值加1
int sp_thread_semaphore_post(sp_thread_semaphore_t * semaphore);
//获得一个信号量，成功返回0，此时信号量的数值减1，否则阻塞
int sp_thread_semaphore_wait(sp_thread_semaphore_t * semaphore);
//获得一个信号量，成功返回0，此时信号量的数值减1，否则等待超时
int sp_thread_semaphore_timedwait( sp_thread_semaphore_t * semaphore,DWORD dwMilliseconds);

int sp_thread_create( sp_thread_t * thread, sp_thread_attr_t * attr,
		sp_thread_func_t myfunc, void * args );
sp_thread_t sp_thread_self();

#ifdef __cplusplus
}
#endif

#endif
