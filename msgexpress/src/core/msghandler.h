#ifndef _MSGHANDLER_H_
#define _MSGHANDLER_H_

#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <crtdbg.h>
#include "spwin32iocp.hpp"
#include "spiocpserver.hpp"
#define _CRTDBG_MAP_ALLOC
#endif
#include <assert.h>

#include "../spserver/spthread.hpp"
#include "../spserver/spporting.hpp"


#include "../spserver/spmsgdecoder.hpp"
#include "../spserver/spbuffer.hpp"

#include "../spserver/sphandler.hpp"
#include "../spserver/spresponse.hpp"
#include "../spserver/sprequest.hpp"
#include "../spserver/sputils.hpp"

#include "messageutil.h"
#include "msgdecoder.h"
#include "servicemanager.h"
#include "../inc/package.h"



class MSGEXPRESS_API MsgHandler : public SP_Handler {
public:
	MsgHandler( ServiceManager * manager );
	virtual ~MsgHandler();

	virtual int start( SP_Request * request, SP_Response * response );

	// return -1 : terminate session, 0 : continue
	virtual int handle( SP_Request * request, SP_Response * response );

	virtual void error( SP_Response * response );

	virtual void timeout( SP_Response * response );

	virtual void close( SP_Response * response );

private:
	bool hasSuperRights;
	bool stoped;
	AppAddress mSid;
	MsgExpress::LoginInfo mInfo;
	std::set<int> mServiceList;//我提供的服务
	ServiceManager * mServiceManager;
	static int mMsgSeq;

	void process( SP_Request * request,Package* package, SP_Response * response);
	void post(SP_Response * response, const char * buffer, size_t size, AppAddress toSid);
	void post(SP_Response * response, const char * buffer, size_t size, unordered_set<unsigned int>* setIds);
	
	void broadcast(SP_Response * response, const char * buffer, size_t size, AppAddress * ignoreSid = 0);
	void broadcast(SP_Response * response, const string& msg, AppAddress * ignoreSid = 0);

	//void regService(Package* package, SP_Response * response);
	void createprocess(Package* package, SP_Response * response);
	void subscribe(Package* package, SP_Response * response);
	void complexSubscribe(Package* package, SP_Response * response);
	void unsubscribe(Package* package, SP_Response * response);
	void regCallback(const CallbackArg&);
	void login( SP_Request * request,Package* package, SP_Response * response);
	void logout( SP_Request * request,Package* package, SP_Response * response);
	void heartBeat( SP_Request * request,Package* package, SP_Response * response);

	void publish(unsigned int cmd,unsigned int serial,unsigned int srcAddr,MsgExpress::PublishData* pubData, SP_Response * response);
	void publish(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response);
	void simplePublish(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response);
	void complexPublish(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response);
	void simplePublish2(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response);
	void simplePublish3(unsigned int cmd,unsigned int serial,unsigned int srcAddr,Package* package, SP_Response * response);

	void reply(Package* package,const Message& replyMsg,SP_Response * response);
	void reply(Package* package,unsigned int errCode,string errMsg,SP_Response * response);
};

class MSGEXPRESS_API MsgCompletionHandler : public SP_CompletionHandler {
public:
	MsgCompletionHandler(){}
	~MsgCompletionHandler(){}
	virtual void completionMessage( SP_Message * msg );
};

class MSGEXPRESS_API MsgHandlerFactory : public SP_HandlerFactory {
public:
	MsgHandlerFactory( ServiceManager * manager ){mServiceManager=manager;}
	virtual ~MsgHandlerFactory(){}

	virtual SP_Handler * create() const{return new MsgHandler(mServiceManager);}

	virtual SP_CompletionHandler * createCompletionHandler() const{return new MsgCompletionHandler();}

private:
	ServiceManager * mServiceManager;
};

#endif