#include "codebase64.h"

CodeBase64::CodeBase64(void)
{
}

CodeBase64::~CodeBase64(void)
{
}
string CodeBase64::Encode(const unsigned char* Data, int DataByte)
{
	//编码表
	const char EncodeTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	//返回值
	string strEncode;
	unsigned char Tmp[4] =
	{
		0
	};
	int LineLength = 0;
	for (int i = 0; i < (int)(DataByte / 3); i++)
	{
		Tmp[1] = *Data++;
		Tmp[2] = *Data++;
		Tmp[3] = *Data++;
		strEncode += EncodeTable[Tmp[1] >> 2];
		strEncode += EncodeTable[((Tmp[1] << 4) | (Tmp[2] >> 4)) & 0x3F];
		strEncode += EncodeTable[((Tmp[2] << 2) | (Tmp[3] >> 6)) & 0x3F];
		strEncode += EncodeTable[Tmp[3] & 0x3F];
		if (LineLength += 4,LineLength == 76)
		{
			strEncode += "\r\n";LineLength = 0;
		}
	}
	//对剩余数据进行编码
	int Mod = DataByte % 3;
	if (Mod == 1)
	{
		Tmp[1] = *Data++;
		strEncode += EncodeTable[(Tmp[1] & 0xFC) >> 2];
		strEncode += EncodeTable[((Tmp[1] & 0x03) << 4)];
		strEncode += "==";
	}
	else if (Mod == 2)
	{
		Tmp[1] = *Data++;
		Tmp[2] = *Data++;
		strEncode += EncodeTable[(Tmp[1] & 0xFC) >> 2];
		strEncode += EncodeTable[((Tmp[1] & 0x03) << 4) | ((Tmp[2] & 0xF0) >> 4)];
		strEncode += EncodeTable[((Tmp[2] & 0x0F) << 2)];
		strEncode += "=";
	}

	return strEncode;
}

string CodeBase64::Decode(const char* Data, int DataByte, int& OutByte)
{
	//解码表
	const char DecodeTable[] =
	{
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,//25
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,//43
		62, // '+'index=43
		0, 0, 0, 63, // '/' index=47
		52, 53, 54, 55, 56, 57, 58, 59, 60, 61, // '0'-'9'
		0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, // 'A'-'Z'
		0, 0, 0, 0, 0, 0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, // 'a'-'z'

	};
	//返回值
	string strDecode;
	unsigned char nValue;
	int i = 0;
	unsigned char s1, s2, s3, s4;
	unsigned char b1, b2, b3, b4;
	while (i < DataByte)
	{
		if (*Data != '\r' && *Data != '\n')
		{
			s1 = *Data;
			Data++;
			s2 = *Data;
			Data++;
			s3 = *Data;
			Data++;
			s4 = *Data;
			Data++;
			if ((s1 < 43 || s1 > 122) || (s2 < 43 || s2 > 122) || (s3 < 43 || s3 > 122) || (s4 < 43 || s4 > 122))
			{
				//int pp = 0;
			}
			b1 = DecodeTable[s1];
			b2 = DecodeTable[s2];
			b3 = DecodeTable[s3];
			b4 = DecodeTable[s4];
			nValue = ((b1 << 2) | (b2 >> 4));
			strDecode += nValue;
			OutByte++;
			if (s3 != '=')
			{
				nValue = ((b2 << 4) | (b3 >> 2));
				strDecode += nValue;
				OutByte++;
				if (s4 != '=')
				{
					nValue = ((b3 << 6) | b4);
					strDecode += nValue;
					OutByte++;
				}
			}
			i += 4;
		}
		else// 回车换行,跳过
		{
			Data++;
			i++;
		}
	}
	return strDecode;
}

