#include "generatedpublish.h"
#include "../inc/logger.h"
#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>


GeneratedPublish::GeneratedPublish(void)
{
}


GeneratedPublish::~GeneratedPublish(void)
{
	std::unordered_map<int,std::vector<std::string>*>::const_iterator iter=mapValues.begin();
	while(iter!=mapValues.end())
	{
		delete iter->second;
		iter++;
	}

}

std::vector<std::string>* GeneratedPublish::GetVector(int fieldNum)
{
	if(mapValues.find(fieldNum)!=mapValues.end())
		return mapValues[fieldNum];
	else
	{
		std::vector<std::string>* vec=new std::vector<std::string>();
		mapValues[fieldNum]=vec;
		return vec;
	}
}

int GeneratedPublish::GetSubInfo(MsgExpress::DataItem*& subInfo)
{
	subInfo=NULL;
	int count=mapValues.size();
	if(count<1)
		return count;
	subInfo=new MsgExpress::DataItem[count];
	count=0;
	std::unordered_map<int,std::vector<std::string>*>::const_iterator iter=mapValues.begin();
	while(iter!=mapValues.end())
	{
		subInfo[count].set_key(iter->first);
		std::vector<std::string>* vec=iter->second;
		for(int i=0;i<vec->size();i++)
			subInfo[count].add_strval(vec->at(i));
		iter++;
		count++;
	}
	return count;
}

int GeneratedPublish::GetSubInfo(MsgExpress::DataItem*& subInfo, const set<int>& keys)
{
	subInfo = NULL;
	int count = mapValues.size();
	if (count<1)
		return count;
	if (keys.size() == 0)
		return 0;
	subInfo = new MsgExpress::DataItem[count];
	count = 0;
	std::unordered_map<int, std::vector<std::string>*>::const_iterator iter = mapValues.begin();
	while (iter != mapValues.end())
	{
		int key = iter->first;
		if (keys.find(key) == keys.end())
		{
			iter++;
			continue;
		}
		subInfo[count].set_key(key);
		std::vector<std::string>* vec = iter->second;
		for (int i = 0; i<vec->size(); i++)
			subInfo[count].add_strval(vec->at(i));
		iter++;
		count++;
	}
	return count;
}

void GeneratedPublish::ParseFromString(const std::string& input)
{
	google::protobuf::io::CodedInputStream* stream=new google::protobuf::io::CodedInputStream((const google::protobuf::uint8*)input.data(),input.size());
	ParseFromCodedStream(stream);
}

void GeneratedPublish::ParseFromCodedStream(google::protobuf::io::CodedInputStream* input)
{
	int count = 0;
	::google::protobuf::uint32 tag;
    while ((tag = input->ReadTag()) != 0) {
		if (count > 5)//最多只处理前面5个字段
			break;
		int fieldNum=::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag);
		google::protobuf::internal::WireFormatLite::WireType type=::google::protobuf::internal::WireFormatLite::GetTagWireType(tag);
		//LOG4_INFO("protobuf, type%d,field:%d", type, fieldNum);
        switch (type) {
		case ::google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT: 
			{
				::google::protobuf::int64 num=0;
				::google::protobuf::internal::WireFormatLite::ReadPrimitive<::google::protobuf::int64, ::google::protobuf::internal::WireFormatLite::TYPE_INT64>(input, &num);
				if (num > -999999999999 && num < 999999999999)
				{
					vecFields.push_back(fieldNum);
					char val[24];
					sprintf(val, "%ld", num);
					std::vector<std::string>* vecValues = GetVector(fieldNum);
					vecValues->push_back(std::string(val));
					count++;
				}
				break;
			}
		case ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED32: 
			{
				float num=0.0f;
				::google::protobuf::internal::WireFormatLite::ReadPrimitive<float, ::google::protobuf::internal::WireFormatLite::TYPE_FLOAT>(input, &num);
				if (num > -999999999999 && num < 999999999999)
				{
					//vecFields.push_back(fieldNum);
					//char val[24];
					//sprintf(val, "%.4f", num);
					//std::vector<std::string>* vecValues=GetVector(fieldNum);
					//vecValues->push_back(std::string(val));
				}
				break;
			}
		case ::google::protobuf::internal::WireFormatLite::WIRETYPE_FIXED64: 
			{
				double num=0;
				::google::protobuf::internal::WireFormatLite::ReadPrimitive<double, ::google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(input, &num);
				if (num > -999999999999 && num < 999999999999)
				{
					//vecFields.push_back(fieldNum);
					//char val[24];
					//sprintf(val, "%.4f", num);
					//std::vector<std::string>* vecValues=GetVector(fieldNum);
					//vecValues->push_back(std::string(val));
				}
				break;
			}
		case ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED: 
			{
				std::string str;
				::google::protobuf::internal::WireFormatLite::ReadString(input, &str);
				if (str.size() < 64)
				{
					vecFields.push_back(fieldNum);
					std::vector<std::string>* vecValues = GetVector(fieldNum);
					vecValues->push_back(str);
					count++;
				}
				break;
			}
		default:
			{
			return;
			}
		}
	}
}