#include "xmlconfig.h"
#include "../inc/logger.h"
#ifdef __linux__
#include <unistd.h> 
#endif
string XMLConfig::mPath="";

XMLConfig::XMLConfig()
{
	mDocument=NULL;
	loaded=false;
}
XMLConfig::~XMLConfig()
{
	delete mDocument;
}
void XMLConfig::SetPath(const string& path)
{
	mPath=path;
}
bool XMLConfig::LoadFile(string file)
{
	bool ret=false;
	std::unique_lock<mutex> lock(mtx);
	do
	{
		if(loaded)
		{
			LOG4_INFO("Xml file has been loaded, %s",file.c_str());
			ret= true;
			break;
		}
	#ifdef _WIN32
		string xml_file;
		if(mPath.size()==0)
		{
			//get path
			wchar_t _szFilePath[MAX_PATH];
			//获得当前目录
			GetCurrentDirectory(MAX_PATH,_szFilePath);
			wchar_t* p = wcsrchr(_szFilePath, L'\\');
			*p = 0;
			//获得与当前目录平级的config目录
			char * pcstr = (char *) new char[2 *wcslen(_szFilePath) + 1];
			WideCharToMultiByte(CP_OEMCP,NULL,_szFilePath,-1,pcstr,MAX_PATH,NULL,FALSE);
			//xml
			xml_file=pcstr;
			xml_file += "\\config\\" + file;
			delete []pcstr;
		}
		else
		{
			if(mPath.at(mPath.size()-1)=='\\' || mPath.at(mPath.size()-1)=='/')
				xml_file=mPath+file;
			else
				xml_file=mPath+'\\'+file;
		}
		LOG4_INFO("xml path: %s",xml_file.c_str());
	#elif __linux__
		string xml_file;
		if(mPath.size()==0)
		{
			//get path 
			#define MAX_PATH 216
			char buffer[MAX_PATH];
			char* path =  getcwd(buffer ,MAX_PATH);
			if(path == NULL)
			{   
				LOG4_ERROR("can't get cur path");
				break;
			}  
			char * p = strrchr(path,'/');
			if(p == NULL)
			{   
					break;
			}   
			*p = '\0';
			char * tmp = strrchr(path,'/');
			if(tmp == NULL)
				  break;
			*p = '\0';
			xml_file=path;
			xml_file +="/config/" + file;
		}
		else
		{
			if(mPath.at(mPath.size()-1)=='\\' || mPath.at(mPath.size()-1)=='/')
				xml_file=mPath+file;
			else
				xml_file=mPath+'/'+file;
		}
		LOG4_INFO("xml path %s",xml_file.c_str());
	#endif
		delete mDocument;
		mDocument = new TiXmlDocument(xml_file.c_str());
		if(mDocument == NULL)
		{
			LOG4_ERROR("new xmldoc failed, %s",xml_file.c_str());
			break;
		}
		if(!mDocument->LoadFile())
		{
			LOG4_ERROR("Can't load %s",xml_file.c_str());
			break;
		}
		ret=true;
		if(!loaded)
		    loaded= ReadConfigFile();
	}while(false);
	return ret & loaded;
}
