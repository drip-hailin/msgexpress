

#ifndef __ioutils_h__
#define __ioutils_h__

#include "porting.h"

class IOUtils {
public:
	static void inetNtoa( in_addr * addr, char * ip, int size );
	static int setNonblock( int fd );
	static int setBlock( int fd );
	static int setNodelay(int fd );
	static int setBuffer(intptr_t fd,unsigned int sndbuf=10240,unsigned int recvbuf=10240 );
	static int setTimeout(intptr_t fd, unsigned char sndtimeout = 3, unsigned char recvtimeout = 3);//��λ��
	static int tcpListen( const char * ip, int port, int * fd, int blocking = 1 );
	static int initDaemon( const char * workdir = 0 );
	static int tcpListen( const char * path, int * fd, int blocking = 1, int mode = 0666 );
private:
	IOUtils();
};

#endif

