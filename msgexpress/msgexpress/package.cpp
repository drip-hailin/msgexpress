
#include "package.h"

Package::Package(const PackageHeader& header,std::string& classType,const char* data,size_t size,MessagePtr pMsg)
{
	memcpy(&m_Header,&header,PackageHeaderLength);
	m_pMsg=pMsg;
	m_Type=classType;
	m_Content=NULL;
	if(size>0)
	{
		m_Content=new char[size];
	    memcpy(m_Content,data,size);
	}
	m_Size=size;
}

Package::~Package()
{
	delete[] m_Content;
}

void Package::setSrcAddr(unsigned int addr)
{
	m_Header.srcaddr=addr;
	if(m_Content)
		m_Header.write(m_Content);
}
void Package::setDstAddr(unsigned int addr)
{
	m_Header.dstaddr=addr;
	if(m_Content)
		m_Header.write(m_Content);
}