#ifndef _USERLIST_H_
#define _USERLIST_H_

#ifdef _WIN32 
#include <winsock2.h>
#include <windows.h>
#endif
#include <stdio.h>
#include <assert.h>

#include "spresponse.hpp"
#include "spthread.hpp"

class UserList {
public:
	UserList();
	~UserList();

	void copy( SP_SidList * outList, SP_Sid_t * ignoreSid = NULL );
	void remove( SP_Sid_t sid );
	void add( SP_Sid_t sid );
	int getCount();
	SP_Sid_t getRandUser();
private:
	SP_SidList mList;
	sp_thread_mutex_t mMutex;
};

#endif