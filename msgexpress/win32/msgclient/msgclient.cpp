#include "msgclient.h"


SP_Client* SP_Client::me = NULL;


SP_Client::SP_Client(){ 
	host = "127.0.0.1";
	port = 23;
	messageList = list<LPONMESSAGE>();
	eventList = list<LPONEVENT>();
};

SP_Client::~SP_Client(){
	messageList.clear();
	eventList.clear();
}

SP_Client* SP_Client::Get_SP_Client(){
	if (SP_Client::me == NULL){
		SP_Client::me = new SP_Client();
	}
	return SP_Client::me;
}


///@brief 客户端初始化函数,
BOOL SP_Client::InitializeClient(){

	if (hIocp != NULL && client->mFd != NULL){
		return true;
	}


	WSADATA wsaData;
	int err = WSAStartup( MAKEWORD( 2, 0 ), &wsaData );
	if ( err != 0 ) {
		printf( "Couldn't find a useable winsock.dll.\n" );
		return -1;
	}

	hIocp = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, 0, 0 );
	if( NULL == hIocp ) {
		printf( "CreateIoCompletionPort failed, errno %d\n", WSAGetLastError() );
		return -1;
	}

	client = (SP_TestClient*)malloc(sizeof( SP_TestClient ) );

	struct sockaddr_in sin;
	memset( &sin, 0, sizeof(sin) );
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = inet_addr( host );
	sin.sin_port = htons( port );

	printf( "Create %d connections to server, it will take some minutes to complete.\n", 1 );

	memset( client, 0, sizeof( SP_TestClient ) );

	client->mFd = socket( AF_INET, SOCK_STREAM, 0 );
	if( client->mFd < 0 ) {
		printf( "socket failed, errno %d, %s\n", errno, strerror( errno ) );
		spwin32_pause_console();
		return -1;
	}

	if( connect( client->mFd, (struct sockaddr *)&sin, sizeof(sin) ) != 0) {
		printf( "connect failed, errno %d, %s\n", errno, strerror( errno ) );
		spwin32_pause_console();
		return -1;
	}

	if( NULL == CreateIoCompletionPort( (HANDLE)client->mFd, hIocp, (DWORD)client, 0 ) ) {
		printf( "CreateIoCompletionPort failed, errno %d\n", WSAGetLastError() );
		return -1;
	}

	//添加线程池，装载流程
	SP_ThreadPool pool( 2 );
	pool.dispatch(messageRead, hIocp);
}

void SP_Client::messageRead( HANDLE hIocp )
{
	DWORD bytesTransferred = 0;
	SP_TestClient * client = NULL;
	LPOVERLAPPED overlapped = NULL;

	for(;;)
	{
		BOOL isSuccess = GetQueuedCompletionStatus( hIocp, &bytesTransferred,
			(DWORD*)&client, &overlapped, 100 );
		DWORD lastError = WSAGetLastError();

		if( ! isSuccess ) {
			if( NULL != client ) {
				me->gStat.mGQCSFail++;
				close_client( client );
			}
			continue;
		}
		printf( "thread#%ld\n", sp_thread_self() );

		me->on_read(bytesTransferred);

	}
}


void SP_Client::on_read(DWORD bytes)
{
	char buffer[ 4096 ] = { 0 };
	int bytesTransferred = recv( (int)client->mFd, buffer, sizeof( buffer ), 0 );

	if( bytesTransferred <= 0 ) {
		if( bytesTransferred < 0 ) {
			printf( "recv fail, errno %d\n", WSAGetLastError() );
			gStat.mRecvFail++;
		}
		close_client( client );
		return;
	}

	databuff.append(buffer,bytesTransferred);
	int ret=decoder.decode(&databuff);
	if(ret==SP_MsgDecoder::eOK)
	{
		SP_BlockingQueue* queue=decoder.getQueue();
		Package* package=NULL;
		while(package=(Package*)queue->pop())
		{
			if(package->getMessage().get())
			{
				unsigned char type=package->getPackageType();
				//if(type==PackageHeader::Publish)
				//{
				//	onPublish(package->getMessage());
				//}
				//else if(type==PackageHeader::Response)
			 //       printf("Response:%s\r\n",package->getMessage()->ShortDebugString().c_str());
				//else if(type==PackageHeader::Request)
				//{
				//	Message* resp=NULL;
				//	onRequest(package->getMessage(),resp);
				//	reply(client,package,resp);
				//}
			}
		}
	}
}

///@brief 客户端释放函数,
BOOL SP_Client::ReleaseClient(){
			
		closeClients( client,1);
		CloseHandle( hIocp );
		return TRUE;
}
///@brief 注册一个回调函数
BOOL SP_Client::RegisterMsgCallBack(LPONMESSAGE pOnMessage)
{
	messageList.push_back(pOnMessage);
	return false;
}
///@brief 反注册一个用于接收某类消息的回调函数
BOOL SP_Client::UnRegisterMsgCallBack(LPONMESSAGE pOnMessage)
{
	messageList.remove(pOnMessage);
	return false;
}
///@brief 注册一个回调函数
BOOL SP_Client::RegisterEventCallBack(LPONEVENT pOnMessage)
{
	eventList.push_back(pOnMessage);
	return false;
}
///@brief 反注册一个用于接收某类消息的回调函数
BOOL SP_Client::UnRegisterEventCallBack(LPONEVENT pOnMessage)
{
	eventList.remove(pOnMessage);
	return false;
}
///@brief 客户端发送异步消息的接口
unsigned int SP_Client::PostMessage(MessagePtr msg)
{
	return -1;
}
///@brief 客户端发送同步消息的接口
BOOL SP_Client::SendMessage(MessagePtr request, MessagePtr response,unsigned int unTimeOut){
	return false;
}

//