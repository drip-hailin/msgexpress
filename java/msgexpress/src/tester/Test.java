package tester;

import com.config.ClientCfg;
import com.databus.*;
import com.packet.PackageData;
import com.service.Provider;
import com.databus.*;
import com.protobuf.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@ServiceInfo()
public class Test extends Provider {

    @PublishMapping
    public com.protobuf.Test.News news;

    public void test() {
        com.protobuf.Test.Faq.Builder buider = com.protobuf.Test.Faq.newBuilder();
        buider.setQuestion("hello");
        com.protobuf.Test.Faq faq = buider.build();
        HelloWorld hello=new HelloWorld();
        hello.setHello("hello");
        this.Publish(hello);
        //this.Publish(faq, null, null, 3000);
//		FutureResponse<Object> reply=SendMessage(new HelloWorld());
//		if(reply!=null) {
//			Object obj= null;
//			try {
//				obj = reply.get();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			} catch (ExecutionException e) {
//				e.printStackTrace();
//			}
//			if(obj!=null)
//		    	System.out.println(obj);
//		}
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        ClientCfg cfg = new ClientCfg();
        String path = System.getProperty("user.dir");
        //cfg.LoadConfig(path+"\\config");
        cfg.AddBrokerAddress("47.100.7.224", 7777);
        Test test = new Test();
        test.Initialize(cfg);

        while (true) {
            try {
                Thread.sleep(1000);
               test.test();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }

    @RequestMapping(url="helloworld")
    public HelloWorld handleHelloworld(HelloWorld hello) {
        Log.info(hello.toString());
        hello.setHello("Hello the world!!!");
        return hello;
    }

    @RequestMapping
    public com.protobuf.Test.Faq handleFaq(com.protobuf.Test.Faq faq) {
        Log.info(faq.toString());
        return com.protobuf.Test.Faq.newBuilder().mergeFrom(faq).setAnswer(faq.getQuestion()).build();
    }

    @RequestMapping
    public com.protobuf.Test.Hello handleHello(com.protobuf.Test.Hello hello) {
        Log.info(hello.toString());
        return hello;
        //return com.protobuf.Test.Faq.newBuilder().mergeFrom(faq).setAnswer(faq.getQuestion()).build();
    }

    @RequestMapping
    public void hello(String hi)
    {

    }

    @PublishMapping
    public void onFaq(com.protobuf.Test.Faq faq) {
        Log.info("publish:" + faq.toString());
    }

    @PublishMapping(params = "")
    public Object handlePublish(com.protobuf.Test.Hello hello) {

        Log.info(hello.toString());
        return null;
    }
}