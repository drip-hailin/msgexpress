// 信息统计接口
package com.databus;

public interface InfoCount {

	// 获取发送队列大小
	public int GetSendQueue();
	
	// 获取接收队列大小
	public int GetReceiveQueue();
	
	// 获取发送请求数
	public int GetSendRequest();
	
	// 获取接收请求数
	public int GetRecvRequest();
	
	// 获取发送应答数
	public int GetSendResponse();
	
	// 获取接收应答数
	public int GetRecvResponse();
	
	// 获取发送通知数
	public int GetSendPub();
	
	// 获取接收通知数
	public int GetRecvPub();
	
}
