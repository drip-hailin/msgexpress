package com.databus;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceInfo {
    int serviceid() default 0;
    String servicename() default "";
}
