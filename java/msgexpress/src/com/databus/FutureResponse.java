package com.databus;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class FutureResponse<V> implements Future<V> {
    private V val=null;
    private Object obj=null;
    private int serial=0;
    private SyncMessageArray syncObj=null;
    public void setSerail(int serial)
    {
        this.serial=serial;
    }
    public void setSyncObj(SyncMessageArray syncObj){
        this.syncObj=syncObj;
    }
    public void setValue(V val){
        this.val=val;
    }
    @Override
    public boolean cancel(boolean b) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return val!=null;
    }

    @Override
    public V get() throws InterruptedException, ExecutionException
    {
        val=(V)syncObj.WaitForResponse(serial,10000);
        return val;
    }

    @Override
    public V get(long l, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        Object obj=syncObj.WaitForResponse(serial,l);
        val=(V)obj;
        return val;
    }
}
