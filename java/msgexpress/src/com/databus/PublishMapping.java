package com.databus;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PublishMapping {
    String params() default "";
}
