// 请求/应答包数据类
package com.packet;

import com.google.protobuf.GeneratedMessage;

public class PackageData {
	public PackageHeader mHeader = null;       // 消息头
	public Object mMsg = null;       // 主体消息
	public Object mExtMsg = null;    // 扩展消息
	public byte[] mData = null;                // 消息二进制数据
}
